//
//  AccountListPresenter.swift
//  M.Agent
//
//  Created by Владимир on 18/03/2019.
//Copyright © 2019 Wolfram. All rights reserved.
//

import Foundation

struct AccountListPresenterSetup: PresenterSetupProtocol {
    let someImportantParam: String
    let okFeedback: FeedbackProtocol
    let closeFeedback: FeedbackProtocol
}

protocol AccountListPresenterAPI: PresenterAPIProtocol {
    func showSecondModule()
    func refreshTableState()
}


final class AccountListPresenter: BaseTablePresenter, PresenterSetuperProtocol {
    
    typealias PresenterSetupStruct = AccountListPresenterSetup
    
    fileprivate var closeFeedback: FeedbackProtocol?
    fileprivate var okFeedback: FeedbackProtocol?
    
    override func setupView(data: Any) {
        if let setup = data as? AccountListPresenterSetup
        {
            closeFeedback = setup.closeFeedback
            okFeedback = setup.okFeedback
        }
    }
    
    func reloadData() {
        self.viewHasLoaded()
    }
    override func buildSections() -> [TableSectionViewModel] {
        // common views' data setup
        
        view.myDisplayData.mainTitle = ""
        
        view.myDisplayData.addCommand = Command(command: {[weak self] in
            self?.router.showAddAccount(setupData: AccountAddPresenterSetup(okFeedback: FeedbackHolder(feedbackID: "addAccountDone", feedbackBlock: { (presenter, string) in
                if presenter is AccountAddPresenter {
                    self?.reloadData()
                }
            }), closeFeedback: self!.router))
            
        })
        
        // table view data setup
        
        let s1 = WFTableSectionModel(titleForHeader: "Пользователи", titleForFooter: "")
        for account in interactor.accounts{
            let cellModel = BasicCells.TwoLabelCellModel(oneLabelText: account.registration_date,
                                                         twoLabelText: account.name + "\n" +
                                                                      (account.note ?? ""))
            cellModel.selectCommand = TableSectionViewModelCommand(sectionViewModel: s1, perform: { (_) in
                self.router.showDetailAccount(id: account.id)
            })
            s1.rows.append(cellModel)
        }
        return [s1]
    }
    
    override func viewIsAboutToDisappear() {
        if let feedBack = closeFeedback {
            feedBack.receiveFeedbackFrom(self, forFeedbackID: feedBack.feedbackID)
        }
    }
}

// MARK: - Local API
extension AccountListPresenter: AccountListPresenterAPI {
    func showSecondModule() {
        /*
         router.showSecondModule(with:
         SecondPresenterSetup(someImportantParam: "someImportantParam",
         okFeedback: FeedbackHolder(feedbackID: "expectedOKFeedback") { (presenter, forFeedbackID) in
         
         },
         closeFeedback: router))
         */
    }
    func refreshTableState() {
        view.deselectSelectedRow()
    }
}


// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension AccountListPresenter {
    var view: AccountListViewInterface {
        return _view as! AccountListViewInterface
    }
    var interactor: AccountListInteractor {
        return _interactor as! AccountListInteractor
    }
    var router: AccountListRouter {
        return _router as! AccountListRouter
    }
}


