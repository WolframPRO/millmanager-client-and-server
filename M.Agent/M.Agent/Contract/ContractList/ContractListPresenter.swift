//
//  ContractListPresenter.swift
//  M.Agent
//
//  Created by Владимир on 21/03/2019.
//Copyright © 2019 Wolfram. All rights reserved.
//

import Foundation

struct ContractListPresenterSetup: PresenterSetupProtocol {
    let someImportantParam: String
    let okFeedback: FeedbackProtocol
    let closeFeedback: FeedbackProtocol
}

protocol ContractListPresenterAPI: PresenterAPIProtocol {
    func showSecondModule()
    func refreshTableState()
}


final class ContractListPresenter: BaseTablePresenter, PresenterSetuperProtocol {
    
    typealias PresenterSetupStruct = ContractListPresenterSetup
    
    fileprivate var closeFeedback: FeedbackProtocol?
    fileprivate var okFeedback: FeedbackProtocol?
    var twoDatePickerViewIsShow = false
    var twoDatePickerView: TwoDatePickerView?
    
    override func setupView(data: Any) {
        if let setup = data as? ContractListPresenterSetup
        {
            closeFeedback = setup.closeFeedback
            okFeedback = setup.okFeedback
        }
    }
    func reloadData() {
        self.viewHasLoaded()
    }
    
    override func buildSections() -> [TableSectionViewModel] {
        // common views' data setup
        
        view.myDisplayData.mainTitle = ""
        
        DispatchQueue.main.async { [weak self] in
            self?.twoDatePickerView = TwoDatePickerView(accounts: self?.interactor.profiles ?? [], completion: {[weak self] (elid) in
                self?.twoDatePickerViewIsShow = false
                guard elid.count > 0 else {
                    return
                }
                self?.interactor.createContract(elid: elid, completion: { (done) in
                    if done {
                        self?.reloadData()
                    } else {
                        self?.view.errorCreate()
                    }
                })
            })
            
            self?.view.myDisplayData.addCommand = Command(command: {[weak self] in
                if !self!.twoDatePickerViewIsShow {
                    self?.twoDatePickerView?.showFrom(view: self!._view.view)
                    self?.twoDatePickerViewIsShow = true
                } else {
                    self?.twoDatePickerView?.closeSelf()
                    self?.twoDatePickerViewIsShow = false
                }
            })
        }
        
        // table view data setup
        
        let s1 = WFTableSectionModel(titleForHeader: "Договоры", titleForFooter: "")
        for contract in interactor.contracts{
            let cellModel = BasicCells.TwoLabelCellModel(oneLabelText: contract.contract_name, twoLabelText: contract.company_name + "\n" + contract.client_name + "\nСтатус договора: " + contract.status)
            //cellModel.selectCommand = TableSectionViewModelCommand(sectionViewModel: s1, perform: { (_) in
            //    self.router.showDetailAccount(id: contract.id)
            //})
            s1.rows.append(cellModel)
        }
        return [s1]
    }
    
    override func viewIsAboutToDisappear() {
        if let feedBack = closeFeedback {
            feedBack.receiveFeedbackFrom(self, forFeedbackID: feedBack.feedbackID)
        }
    }
}

// MARK: - Local API
extension ContractListPresenter: ContractListPresenterAPI {
    func showSecondModule() {
        /*
         router.showSecondModule(with:
         SecondPresenterSetup(someImportantParam: "someImportantParam",
         okFeedback: FeedbackHolder(feedbackID: "expectedOKFeedback") { (presenter, forFeedbackID) in
         
         },
         closeFeedback: router))
         */
    }
    func refreshTableState() {
        view.deselectSelectedRow()
    }
}


// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension ContractListPresenter {
    var view: ContractListViewInterface {
        return _view as! ContractListViewInterface
    }
    var interactor: ContractListInteractor {
        return _interactor as! ContractListInteractor
    }
    var router: ContractListRouter {
        return _router as! ContractListRouter
    }
}


