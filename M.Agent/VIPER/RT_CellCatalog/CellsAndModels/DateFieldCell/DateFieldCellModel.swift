//
//  TwoLabelCellModel.swift
//  RT_CellCatalogPod
//
//  Created by Вова Петров on 26/12/2018.
//  Copyright © 2018 DartIT. All rights reserved.
//

import UIKit

public enum DateFieldCellModelType: CellModelTypeProtocol {
    case standart
    public var nibNameAndReuseIdentifier: (String, String) {
        switch self {
            case .standart:
                return ("DateFieldCell", "DateFieldCell")
        }
    }
}

public extension BasicCells {
    
    public final class DateFieldCellModel: BaseCellModel {
        
        public var labelText: String
        public var fieldDate: Date?
        public var fieldPlaceholderText: String
        public var minDate: Date?
        public var maxDate: Date?
        public var dateFormat = "dd.MM.yyyy"
        public var datePickerMode = UIDatePicker.Mode.date
        public var showToolbar = true
        public var showTodayButton = true
        
        public var selectedBlock: (Date)->()
        public var beginEditingBlock: (_ keyboardHeight: CGFloat)->()
        public var endEditingBlock: ()->()
        
        public init(labelText: String, fieldPlaceholderText: String, type: DateFieldCellModelType = .standart, style: CellModelStyle? = nil, selectedBlock: @escaping (Date)->(), beginEditingBlock: @escaping (_ keyboardHeight: CGFloat)->(), endEditingBlock: @escaping ()->()) {
            self.labelText = labelText
            self.fieldPlaceholderText = fieldPlaceholderText
            self.selectedBlock = selectedBlock
            self.beginEditingBlock = beginEditingBlock
            self.endEditingBlock = endEditingBlock
            
            super.init(reuseIdentifier: type.nibNameAndReuseIdentifier.1)
            initStyle(style)
            initTypes(with: DateFieldCellModelType.allCases)
        }
    }
}
