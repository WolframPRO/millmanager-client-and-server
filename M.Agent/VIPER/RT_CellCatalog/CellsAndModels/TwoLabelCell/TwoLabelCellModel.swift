//
//  TwoLabelCellModel.swift
//  RT_CellCatalogPod
//
//  Created by Вова Петров on 26/12/2018.
//  Copyright © 2018 DartIT. All rights reserved.
//

import UIKit

public enum TwoLabelCellModelType: CellModelTypeProtocol {
    ///Если оставить twoLabelText пустым создастся ячейка с одним Label и констрейнты перестроятся
    case horizontal
    case vertical
    case value
    public var nibNameAndReuseIdentifier: (String, String) {
        switch self {
        case .horizontal:
            return ("TwoLabelCellHorizontal", "TwoLabelCellHorizontal")
        case .vertical:
            return ("TwoLabelCell", "twoLabelCell")
        case .value:
            return ("TwoLabelCellValue", "TwoLabelCellValue")
        }
    }
}

public extension BasicCells {
    public final class TwoLabelCellModel: BaseCellModel {

        public var oneLabelText: String
        public var twoLabelText: String
        public private(set) var type: TwoLabelCellModelType
        
        public init(oneLabelText: String, twoLabelText: String, type: TwoLabelCellModelType = .vertical, style: CellModelStyle? = nil) {
            self.oneLabelText = oneLabelText
            self.twoLabelText = twoLabelText
            self.type = type
            
            super.init(reuseIdentifier: type.nibNameAndReuseIdentifier.1)
            initStyle(style)
            initTypes(with: TwoLabelCellModelType.allCases)
        }
    }
}
