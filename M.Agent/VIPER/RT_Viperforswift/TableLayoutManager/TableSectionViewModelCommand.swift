//
//  TableSectionViewModelCommand.swift
//  MyExample
//
//  Created by Alexey Bykov on 12/10/18.
//

import Foundation
import UIKit

open class TableSectionViewModelCommand {
    public private(set) weak var sectionViewModel: TableSectionViewModel?
    private var performBlock: (_ command: TableSectionViewModelCommand?) -> Void
    
    public init(sectionViewModel: TableSectionViewModel, perform performBlockToCall: @escaping (_ command: TableSectionViewModelCommand?) -> Void) {
        self.sectionViewModel = sectionViewModel
        self.performBlock = performBlockToCall
    }
    
    public func perform() {
        weak var weakSelf = self
        self.performBlock(weakSelf)
    }
    
}

