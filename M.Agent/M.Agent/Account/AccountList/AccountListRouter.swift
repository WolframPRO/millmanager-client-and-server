//
//  AccountListRouter.swift
//  M.Agent
//
//  Created by Владимир on 18/03/2019.
//Copyright © 2019 Wolfram. All rights reserved.
//

import Foundation


final class AccountListRouter: RouterExtended {
    /*
     func showSecondModule(with setupData: SecondPresenterSetup) {
     
     setupAndShow(module: AppModules.second,
     with: setupData,
     for: SecondPresenter.self)
     
     }
     */
    
    func showDetailAccount(id: String) {
        setupAndShow(module: AppModules.AccountEdit, with: AccountEditPresenterSetup.init(elid: id, closeFeedback: FeedbackHolder(feedbackID: "", feedbackBlock: {[weak self] (presenter, str) in
            self?.presenter.reloadData()
        })), for: AccountEditPresenter.self)
    }
    func showAddAccount(setupData: AccountAddPresenterSetup) {
        setupAndShow(module: AppModules.AccountAdd,
                     with: setupData,
                     for: AccountAddPresenter.self)
    }
}

extension AccountListRouter: FeedbackProtocol {
    var feedbackID: String {
        return "expectedFeedbackInRouter"
    }
    func receiveFeedbackFrom(_ presenter: PresenterAPIProtocol, forFeedbackID: String) {
        
         if let _ = presenter as? AccountAddPresenter {
            if forFeedbackID == "addComplete" {
                self.presenter.reloadData()
            }
         }
        self.presenter.refreshTableState()
    }
}


// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension AccountListRouter {
    var presenter: AccountListPresenter {
        return _presenter as! AccountListPresenter
    }
}


