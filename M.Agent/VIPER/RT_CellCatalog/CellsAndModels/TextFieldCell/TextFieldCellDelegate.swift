//
//  TextFieldCellDelegate.swift
//  M.Agent
//
//  Created by Владимир on 17/03/2019.
//  Copyright © 2019 Wolfram. All rights reserved.
//

import Foundation
import UIKit

class TextFieldCellDelegate: NSObject, UITextFieldDelegate {
    var textChangeCommand: (_ text: String) -> Bool
    
    init(changeCommand: @escaping (_ text: String) -> Bool) {
        self.textChangeCommand = changeCommand
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return textChangeCommand(textField.text!)
    }

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        let _ = textChangeCommand(textField.text!)
    }
}
