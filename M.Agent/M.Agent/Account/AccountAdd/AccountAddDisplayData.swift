//
//  AccountAddDisplayData.swift
//  M.Agent
//
//  Created by Владимир on 18/03/2019.
//Copyright © 2019 Wolfram. All rights reserved.
//

import Foundation

final class AccountAddDisplayData: BaseTableDisplayData {
    var mainTitle: String?
    var doneCommand: Command?
}
