//
//  CellModelTypes.swift
//  RT_CellCatalogPod
//
//  Created by Alexey Bykov on 2/1/19.
//  Copyright © 2019 Rostelecom. All rights reserved.
//

import Foundation
import UIKit

public protocol CellModelTypeNamesProtocol {
    var nibNameAndReuseIdentifier: (String, String) {get}
}
public protocol CellModelTypeProtocol: CaseIterable, CellModelTypeNamesProtocol {
}

class CellModelTypes: NSObject {
    
    typealias CellModelTypesDict = [String : (String, String)]
    
    static private var allNamesAndIDs: CellModelTypesDict = [:]
    static func register<T: CellModelTypeNamesProtocol>(types: [T]) {
        for type in types {
            allNamesAndIDs[type.nibNameAndReuseIdentifier.0 + type.nibNameAndReuseIdentifier.1] = type.nibNameAndReuseIdentifier
        }
    }
    static var allNibNameAndReuseIdentifiers: CellModelTypesDict.Values {
        return allNamesAndIDs.values
    }
    static func registerAllCells(tableView: UITableView?) {
        var podBundle: Bundle? = nil
        if let podBundleUrl = Bundle.init(for: self.classForCoder()).url(forResource: "RT_CellCatalog", withExtension: "bundle") {
            podBundle = Bundle.init(url: podBundleUrl)
        }
        tableView?.registerAllCells(bundle: podBundle)
    }
}

private extension UITableView {
    func registerAllCells(bundle: Bundle?) {
        let currentBundle = bundle ?? Bundle.main
        for nibNameAndReuseIdentifier in CellModelTypes.allNibNameAndReuseIdentifiers {
            if currentBundle.path(forResource: nibNameAndReuseIdentifier.0, ofType: "nib") != nil
            {
                self.register(UINib.init(nibName: nibNameAndReuseIdentifier.0, bundle: bundle),
                              forCellReuseIdentifier: nibNameAndReuseIdentifier.1)
            }
        }
    }
    
}
