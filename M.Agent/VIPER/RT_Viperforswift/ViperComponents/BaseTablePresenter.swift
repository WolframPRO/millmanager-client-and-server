//
//  BaseTablePresenter.swift
//  Example
//
//  Created by Alexey Bykov on 12/4/18.
//

import Foundation
import Viperit

open class BaseTablePresenter: Presenter, TablePresenterBuilder {
    
    override open func viewHasLoaded() {
        loadContent()
    }
    
    public func reloadView() {
        DispatchQueue.global().async {
            (self._view as! BaseTableView).tableDisplayData().setupSections(self.buildSections())
            DispatchQueue.main.async {
                (self._view as! BaseTableView).update()
            }
        }
    }

    open func buildSections() -> [TableSectionViewModel] {
        return []
    }

}

private extension BaseTablePresenter {
    func loadContent() {
        (_view as! BaseTableView).showLoading()
        (_interactor as! BaseTableInteractor).load()
    }
}


