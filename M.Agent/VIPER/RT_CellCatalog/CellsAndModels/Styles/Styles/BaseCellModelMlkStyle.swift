//
//  BaseCellModelMlkStyle.swift
//  RT_CellCatalogPod
//
//  Created by Alexey Bykov on 1/30/19.
//  Copyright © 2019 DartIT. All rights reserved.
//

import Foundation
import UIKit

public final class BaseCellModelMlkStyle: CellModelStyleBase {
    required public init(with styleToBeDecorated: CellModelStyleProtocol?) {
        super.init(with: styleToBeDecorated)
    }
    override public func imbue() {
        super.imbue()
        // my imbue
        if let cell = self.model?.cell as? UITableViewCell {
            cell.contentView.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        }
    }
}
