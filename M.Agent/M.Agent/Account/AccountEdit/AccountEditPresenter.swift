//
//  AccountEditPresenter.swift
//  M.Agent
//
//  Created by Владимир on 18/03/2019.
//Copyright © 2019 Wolfram. All rights reserved.
//

import Foundation
import CodableFirebase

struct AccountEditPresenterSetup: PresenterSetupProtocol {
    let elid: String
    let closeFeedback: FeedbackProtocol
}

protocol AccountEditPresenterAPI: PresenterAPIProtocol {
    func showSecondModule()
    func refreshTableState()
}


final class AccountEditPresenter: BaseTablePresenter, PresenterSetuperProtocol {
    
    typealias PresenterSetupStruct = AccountEditPresenterSetup
    
    fileprivate var closeFeedback: FeedbackProtocol?
    
    var elid = ""
    var account: AccountShowModel?
    
    override func setupView(data: Any) {
        if let setup = data as? AccountEditPresenterSetup
        {
            self.elid = setup.elid
            closeFeedback = setup.closeFeedback
        }
    }
    
    //account?.allowdeleteitem
    
    override func buildSections() -> [TableSectionViewModel] {
        // common views' data setup
        
        view.myDisplayData.mainTitle = "Регистрация клиента"
        view.myDisplayData.deleteCommand = Command(command: {[weak self] in
            let alert = UIAlertController(title: "Удаление", message: "Уверены что хотите удалить клиента?", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Удалить", style: .destructive, handler: { (_) in
                self?.interactor.deleteClient(completion: {(done) in
                    if done {
                        self?.closeFeedback?.receiveFeedbackFrom(self!, forFeedbackID: "delete")
                        self?.router.back()
                    } else {
                        self?.view.errorDelete()
                    }
                })
            }))
            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel) { (_) -> Void in
            })
            self?._view.present(alert, animated: true, completion: nil)
        })
        // table view data setup
        let s1 = WFTableSectionModel.init()
        
        if let elid = account?.elid, elid.count > 0 {
            s1.rows.append(BasicCells.TwoLabelCellModel(oneLabelText: "Elid в системе", twoLabelText: elid, type: .horizontal, style: .agent))
        }
        
        if let name = account?.name, name.count > 0 {
            s1.rows.append(BasicCells.TwoLabelCellModel(oneLabelText: "Наименование", twoLabelText: name, type: .horizontal, style: .agent))
        }
        
        if let phone = account?.valid_phone, phone.count > 0 {
            s1.rows.append(BasicCells.TwoLabelCellModel(oneLabelText: "Проверенный номер телефона", twoLabelText: phone, type: .horizontal, style: .agent))
        }
        
        if let name = account?.balance, name.count > 0 {
            s1.rows.append(BasicCells.TwoLabelCellModel(oneLabelText: "Баланс", twoLabelText: name, type: .horizontal, style: .agent))
        }
        
        if let registerDate = account?.registration_date, registerDate.count > 0 {
            s1.rows.append(BasicCells.TwoLabelCellModel(oneLabelText: "Дата и время регистрации", twoLabelText: registerDate, type: .horizontal, style: .agent))
        }
        
        if let ip = account?.registration_ip, ip.count > 0 {
            s1.rows.append(BasicCells.TwoLabelCellModel(oneLabelText: "IP-адрес регистрации", twoLabelText: ip, type: .horizontal, style: .agent))
        }
        
        return [s1]
    }
    
    override func viewIsAboutToDisappear() {
        if let feedBack = closeFeedback {
            feedBack.receiveFeedbackFrom(self, forFeedbackID: feedBack.feedbackID)
        }
    }
}

// MARK: - Local API
extension AccountEditPresenter: AccountEditPresenterAPI {
    func showSecondModule() {
        /*
         router.showSecondModule(with:
         SecondPresenterSetup(someImportantParam: "someImportantParam",
         okFeedback: FeedbackHolder(feedbackID: "expectedOKFeedback") { (presenter, forFeedbackID) in
         
         },
         closeFeedback: router))
         */
    }
    func refreshTableState() {
        view.deselectSelectedRow()
    }
}


// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension AccountEditPresenter {
    var view: AccountEditViewInterface {
        return _view as! AccountEditViewInterface
    }
    var interactor: AccountEditInteractor {
        return _interactor as! AccountEditInteractor
    }
    var router: AccountEditRouter {
        return _router as! AccountEditRouter
    }
}


