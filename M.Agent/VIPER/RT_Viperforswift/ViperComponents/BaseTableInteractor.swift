//
//  BaseTableInteractor.swift
//  Example
//
//  Created by Alexey Bykov on 12/4/18.
//

import Foundation
import Viperit

open class BaseTableInteractor: Interactor {
    
    public func completeLoading() {
        (self._presenter as? BaseTablePresenter)?.reloadView()
    }
    
    open func load() {
        completeLoading()
    }
}
