//
//  TextFieldCellModelAgentStyle.swift
//  RT_CellCatalogPod
//
//  Created by Вова Петров on 07/02/2019.
//  Copyright © 2019 Rostelecom. All rights reserved.
//

import Foundation
import UIKit

public final class TextFieldCellModelAgentStyle: CellModelStyleBase {
    required public init(with styleToBeDecorated: CellModelStyleProtocol?) {
        super.init(with: styleToBeDecorated)
    }
    override public func imbue() {
        super.imbue()
        // my imbue
        if  let model = self.model,
            let cell = self.model?.cell as? TextFieldCell {
            
            if cell.label != nil {
                cell.label.textColor = UIColor.agent.text.common
            }
            cell.textField.textColor = model.isReadOnly ? UIColor.agent.text.readOnly : UIColor.agent.text.common
        }
    }
    
}
