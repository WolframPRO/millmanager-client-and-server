//
//  BaseTableSectionModel.swift
//  RT_CellCatalogPod
//
//  Created by Вова Петров on 26/12/2018.
//  Copyright © 2018 DartIT. All rights reserved.
//

import UIKit

open class WFTableSectionModel: TableSectionViewModel {

    private var allCellsAreRegistered = false
    
    private var _header: String? = nil
    public var header: String? {
        get {
            return _header
        }
        set {
            _header = newValue
            self.headerHasHTML = newValue?.hasHTML() ?? false
        }
    }
    
    public var rows: [BaseCellModel]
    
    private var _footer: String? = nil
    public var footer: String? {
        get {
            return _footer
        }
        set {
            _footer = newValue
            self.footerHasHTML = newValue?.hasHTML() ?? false
        }
    }
    
    private var headerHasHTML = false
    private var footerHasHTML = false
    
    public init(titleForHeader: String? = nil, titleForFooter: String?  = nil) {
        self.rows = []
        super.init()
        self.header = titleForHeader
        self.footer = titleForFooter
    }
    
    /// can be overriden
    open override func numberOfRows() -> Int {
        if !allCellsAreRegistered {
            allCellsAreRegistered = true
            CellModelTypes.registerAllCells(tableView: self.tableView)
        }
        return rows.count
    }
    
    open override func titleForHeader() -> String? {
        if let header = self.header {
            return headerHasHTML ? nil : header
        }
        return nil
    }
    open override func htmlTitleForHeader() -> String? {
        if let header = self.header {
            return headerHasHTML ? header : nil
        }
        return nil
    }
    open override func titleForFooter() -> String? {
        if let footer = self.footer {
            return footerHasHTML ? nil : footer
        }
        return nil
    }
    open override func htmlTitleForFooter() -> String? {
        if let footer = self.footer {
            return footerHasHTML ? footer : nil
        }
        return nil
    }
    
    /// can be overriden
    open override func heightForRow(row: Int) -> CGFloat {
        return rows[row].height
    }
    
    /// must be overriden
    open override func cellForRowAtIndexPath(indexPath: IndexPath) -> UITableViewCell {
        let model = rows[indexPath.row]
        let cell = tableView?.dequeueReusableCell(withIdentifier: model.reuseIdentifier) as? SetModelCellProtocol
        if cell == nil {
            let message = "WFTableSectionModel cellForRowAtIndexPath: cell with reuseIdentifier \(model.reuseIdentifier) for model class \(String(describing: type(of: model))) not found!\n"
            assertionFailure(message)
        }

        cell?.set(model: model)
        
        return cell as! UITableViewCell
    }
    
    /// may be overriden optionally
    open override func didSelectRowAtIndexPath(indexPath: IndexPath) {
        if let command = rows[indexPath.row].selectCommand {
            command.perform()
        } else { //Если команды нет, то снимаем выделение с ячейки
            tableView?.deselectRow(at: indexPath, animated: true)
        }
    }

}
