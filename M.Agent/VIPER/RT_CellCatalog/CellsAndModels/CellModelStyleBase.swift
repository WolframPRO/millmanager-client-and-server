//
//  CellModelStyleProtocol.swift
//  RT_CellCatalogPod
//
//  Created by Alexey Bykov on 1/29/19.
//  Copyright © 2019 DartIT. All rights reserved.
//

import Foundation

//MARK: - Styles

public protocol CellModelStyleProtocol: class {
    func imbue()
    var model:BaseCellModel? {get}
}

//MARK: - Styles decorator

open class CellModelStyleBase: NSObject, CellModelStyleProtocol {
    internal private(set) var styleToBeDecorated:CellModelStyleProtocol?
    public private(set) var model:BaseCellModel? = nil

    required public init(with styleToBeDecorated: CellModelStyleProtocol?) {
        self.styleToBeDecorated = styleToBeDecorated
        if let model = styleToBeDecorated as? BaseCellModel {
            self.model = model
        } else {
            self.model = styleToBeDecorated?.model
        }
    }
    open func imbue() {
        styleToBeDecorated?.imbue()
    }
}

//MARK: - Styles builder

protocol CellModelStyleBuilderProtocol {
    associatedtype SpecialStyleClass: CellModelStyleBase = CellModelStyleBase
    associatedtype BaseStyle: RawRepresentable, CellModelStyleBuilderProtocol where Self.BaseStyle.RawValue == String
    func build(for model: BaseCellModel, applying forClasses:[AnyClass]) -> CellModelStyleProtocol
    func specialStyleClassAndItsBaseStyle() -> (SpecialStyleClass.Type, BaseStyle)?
}

extension CellModelStyleBuilderProtocol where Self: RawRepresentable, Self.RawValue == String {
    func build(for model: BaseCellModel, applying forClasses:[AnyClass]) -> CellModelStyleProtocol {
        return CellModelStyleBase.build(self, model: model, forClasses: forClasses)
    }
    func specialStyleClassAndItsBaseStyle() -> (SpecialStyleClass.Type, BaseStyle)? {
        return nil
    }
}

private extension CellModelStyleBase {
    static func build<T: RawRepresentable & CellModelStyleBuilderProtocol>(_ styleBuilder: T, model: BaseCellModel, forClasses:[AnyClass]) -> CellModelStyleProtocol where T.RawValue == String
    {
        let styleClasses = forClasses.compactMap{ (modelClass) -> AnyClass? in
            let baseStyle = styleBuilder.specialStyleClassAndItsBaseStyle()?.1
            let baseStyleName: String = baseStyle != nil ? (baseStyle?.rawValue)! : styleBuilder.rawValue
            let bundle = Bundle(for: modelClass)
            let bundleName = safeString(bundle.infoDictionary?["CFBundleName"])
            let classNameSuffix = String(describing: modelClass) + baseStyleName.uppercasedFirst + "Style"
            let className = (bundleName + "." + classNameSuffix).replacingOccurrences(of: " ", with: "_")
            var styleClass: AnyClass? = NSClassFromString(className)
            if styleClass == nil {
                let bundleName = "RT_CellCatalog"
                let className = (bundleName + "." + classNameSuffix).replacingOccurrences(of: " ", with: "_")
                styleClass = NSClassFromString(className)
                if styleClass == nil {
                    let message = "CellModelStyleBase build: style class named \(className) not found!\n"
                    assertionFailure(message)
                }
            }
            return styleClass
        }
        var currentStyle: CellModelStyleProtocol = model
        for styleClass in styleClasses.reversed() {
            if let concreteStyleClass = styleClass as? CellModelStyleBase.Type {
                currentStyle = concreteStyleClass.init(with: currentStyle)
            }
        }
        if let concreteSpecialStyleClass = styleBuilder.specialStyleClassAndItsBaseStyle()?.0 {
            currentStyle = concreteSpecialStyleClass.init(with: currentStyle)
        }
        return currentStyle
    }
}

func safeString(_ value: Any?) -> String {
    guard let text = value else { return "" }
    return String(describing: text)
}


