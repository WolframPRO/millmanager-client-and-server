//
//  AccountListDisplayData.swift
//  M.Agent
//
//  Created by Владимир on 18/03/2019.
//Copyright © 2019 Wolfram. All rights reserved.
//

import Foundation

final class AccountListDisplayData: BaseTableDisplayData {
    var mainTitle: String?
    var addCommand: Command?
}

