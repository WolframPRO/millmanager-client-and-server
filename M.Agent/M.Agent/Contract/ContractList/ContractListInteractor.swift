//
//  ContractListInteractor.swift
//  M.Agent
//
//  Created by Владимир on 21/03/2019.
//Copyright © 2019 Wolfram. All rights reserved.
//

import Foundation
import Alamofire
import EVReflection
import CodableFirebase

final class ContractListInteractor: BaseTableInteractor {
    var contracts: [ContractListItem] = []
    var profiles: [ProfileAccount] = []
    
    override func load() {
        manager.request(bill("func=contract"), method: .get).responseJSON { (response) in
            self.contracts = []
            let json = response.result.value as! [Any]
            for i in json {
                do {
                    let contract: ContractListItem = try FirebaseDecoder.init().decode(ContractListItem.self, from: i)
                    self.contracts.append(contract)
                } catch{}
            }
            
            manager.request(bill("func=profile"), method: .get).responseJSON { (response) in
                self.profiles = []
                let json = response.result.value as! [Any]
                for i in json {
                    do {
                        let profile: ProfileAccount = try FirebaseDecoder.init().decode(ProfileAccount.self, from: i)
                        self.profiles.append(profile)
                    } catch{}
                }
                super.load()
            }
        }
    }
    
    func createContract(elid: String, completion: @escaping (Bool) -> Void) {
        manager.request(bill("func=contract.edit&sok=ok"), method: .get, parameters: ["profile":elid]).responseJSON { (response) in
            switch response.result {
            case .success:
                completion(true)
            case .failure:
                completion(false)
            }
            
        }
    }
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension ContractListInteractor {
    var presenter: ContractListPresenter {
        return _presenter as! ContractListPresenter
    }
}


struct ProfileAccount: Codable {
    var registration_date: String //2019-03-18 19:34:09
    var account: String //Иван (client@gmail.com)
    var pneed_activation: String //off
    var need_phone_validate: String //on
    var id: String //7
    var reconciliation: String //off
    var pneed_manual_vetting: String //off
    var profiletype: String //1
    var name: String //Иван Иванович
}
