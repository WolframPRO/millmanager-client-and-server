//
//  DateFieldCellModelAgentStyle.swift
//  RT_CellCatalogPod
//
//  Created by Вова Петров on 07/02/2019.
//Copyright © 2019 Rostelecom. All rights reserved.
//

import Foundation
import UIKit

public final class DateFieldCellModelAgentStyle: CellModelStyleBase {
    required public init(with styleToBeDecorated: CellModelStyleProtocol?) {
        super.init(with: styleToBeDecorated)
    }
    override public func imbue() {
        super.imbue()
        // my imbue
        if  let model = self.model,
            let cell = self.model?.cell as? DateFieldCell {
            
            if cell.label != nil {
                cell.label.textColor = model.isReadOnly ? UIColor.agent.text.readOnly : UIColor.agent.text.common
            }
            if cell.toolBar != nil {
                cell.toolBar!.tintColor = UIColor.agent.button.active
            }
            cell.textField.textColor = model.isReadOnly ? UIColor.agent.text.readOnly : UIColor.agent.text.common
        }
    }
    
}
