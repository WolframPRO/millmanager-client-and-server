//
//  TwoLabelCellModelOnlimeStyle.swift
//  RT_CellCatalogPod
//
//  Created by Alexey Bykov on 2/1/19.
//  Copyright © 2019 Rostelecom. All rights reserved.
//

import Foundation
import UIKit

public final class TwoLabelCellModelOnlimeStyle: CellModelStyleBase {
    required public init(with styleToBeDecorated: CellModelStyleProtocol?) {
        super.init(with: styleToBeDecorated)
    }
    override public func imbue() {
        super.imbue()
        // my imbue
        if  let model = self.model as? BasicCells.TwoLabelCellModel,
            let cell = model.cell as? TwoLabelCell
        {
            cell.oneLabel.textColor = UIColor.gray
            switch model.type {
                case .horizontal:
                    cell.twoLabel.textColor = model.isReadOnly ? UIColor.gray : UIColor.black
                case .vertical:
                    cell.twoLabel.textColor = model.isReadOnly ? UIColor.gray : BaseCellModelOnlimeStyle.onlimeColor
                case .value:
                    cell.twoLabel.textColor = model.isReadOnly ? UIColor.gray : BaseCellModelOnlimeStyle.onlimeColor
            }
        }
    }
    
}
