//
//  ViperitExtendedModule.swift
//  Example
//
//  Created by Alexey Bykov on 11/29/18.
//

import UIKit
import Viperit

//MARK: - ViperitExtendedModule Module Protocol
public protocol ViperitExtendedModule {
    var viewType: ViperitViewType { get }
    var viewName: String { get }
    var viewNameIsExtended: Bool { get }
    func build(bundle: Bundle, deviceType: UIUserInterfaceIdiom?) -> ModuleExtended
}

public extension ViperitExtendedModule where Self: RawRepresentable, Self.RawValue == String {
    
    var viewType: ViperitViewType {
        return .storyboard
    }
    
    var viewName: String {
        return rawValue
    }

    var viewNameIsExtended: Bool {
        return true
    }
    func build(bundle: Bundle = Bundle.main, deviceType: UIUserInterfaceIdiom? = nil) -> ModuleExtended {
        return ModuleExtended.build(self, bundle: bundle, deviceType: deviceType)
    }
}

