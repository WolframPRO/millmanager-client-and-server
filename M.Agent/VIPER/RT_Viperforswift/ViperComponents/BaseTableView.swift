//
//  BaseTableView.swift
//  Example
//
//  Created by Alexey Bykov on 12/4/18.
//

import Foundation
import Viperit

//MARK: - Public Interface Protocol
public protocol BaseTableViewInterface {
    func showLoading()
    func showNoData()
    func showTable()
    func setupNoDataMessage(message: String?)
    func showTableFooterMessage(_ message: String)
    func deselectSelectedRow()
}


//MARK: BaseTableView Class
open class BaseTableView: UserInterface {
    fileprivate let tableLayoutManager: TableLayoutManager = TableLayoutManager()
    @IBOutlet weak public private(set) var loadingLabel: UILabel?
    @IBOutlet weak public private(set) var activityIndicator: UIActivityIndicatorView?
    @IBOutlet weak public private(set) var myTableView: UITableView?
    @IBOutlet weak public private(set) var myTableFooterLabel: UILabel?
    
    fileprivate var useRoundedModeIsPending: Bool = false
    fileprivate var leftMargin: CGFloat = 15
    fileprivate var rightMargin: CGFloat = 15
    
    fileprivate var noDataMessage: String? = nil
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        if useRoundedModeIsPending {
            useRoundedMode(useIt: true, leftMargin: leftMargin, rightMargin: rightMargin)
            useRoundedModeIsPending = false
        }
    }
    
    open func update() {
        showTable()
        if let tableView = myTableView,
           let sections = tableDisplayData().sections
        {
            if sections.count > 0 {
                update(tableView: tableView, sections: sections)
            } else {
                tableLayoutManager.empty(tableView: tableView)
                tableView.reloadData()
                showNoData()
            }
        }
    }
}

//MARK: - Public interface
extension BaseTableView: BaseTableViewInterface {
    public func showTable() {
        loadingLabel?.isHidden = true
        activityIndicator?.stopAnimating()
        myTableView?.isHidden = false
    }
    public func showLoading() {
        loadingLabel?.isHidden = false
        loadingLabel?.text = "Пожалуйста, подождите\nИдет загрузка..."
        activityIndicator?.startAnimating()
        myTableView?.isHidden = true
    }
    public func setupNoDataMessage(message: String?) {
        noDataMessage = message
    }
    public func showNoData() {
        loadingLabel?.isHidden = false
        loadingLabel?.text = noDataMessage ?? "Нет данных"
        activityIndicator?.stopAnimating()
        myTableView?.isHidden = true
    }
    public func showTableFooterMessage(_ message: String) {
        loadingLabel?.isHidden = true
        activityIndicator?.stopAnimating()
        myTableView?.isHidden = false
        myTableFooterLabel?.text = message
    }
    public func deselectSelectedRow() {
        deselect()
    }
}

//MARK: - Public base class interface
public extension BaseTableView {
    
    func deselect() {
        if let tableView = myTableView,
            let indexPath = tableView.indexPathForSelectedRow
        {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func tableDisplayData() -> BaseTableDisplayData {
        return _displayData as! BaseTableDisplayData
    }
    
    func useRoundedMode(useIt: Bool, leftMargin: CGFloat = 15, rightMargin: CGFloat = 15) {
        if let tableView = myTableView {
            tableLayoutManager.useRoundedMode(useIt: useIt, leftMargin: leftMargin, rightMargin: rightMargin, forTableView: tableView)
        } else if useIt {
            useRoundedModeIsPending = true
        }
    }
}

private extension BaseTableView {
    func update(tableView: UITableView, sections: [TableSectionViewModel]) {
        tableLayoutManager.eatSectionModels(sections, forTableView: tableView)
        tableView.reloadData()
    }
}

