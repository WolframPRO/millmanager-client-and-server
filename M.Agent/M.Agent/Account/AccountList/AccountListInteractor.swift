//
//  AccountListInteractor.swift
//  M.Agent
//
//  Created by Владимир on 18/03/2019.
//Copyright © 2019 Wolfram. All rights reserved.
//

import Foundation
import Alamofire
import EVReflection
import CodableFirebase

final class AccountListInteractor: BaseTableInteractor {
    var accounts: [AccountListItem] = []
    
    override func load() {
        

        manager.request(bill("func=account"), method: .get).responseJSON { (response) in
            self.accounts = []
            let json = response.result.value as! [Any]
            for i in json {
                do {
                    let account: AccountListItem = try FirebaseDecoder.init().decode(AccountListItem.self, from: i)
                    self.accounts.append(account)
                } catch{}
            }
            super.load()
        }
        
        //        }
    }
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension AccountListInteractor {
    var presenter: AccountListPresenter {
        return _presenter as! AccountListPresenter
    }
}


