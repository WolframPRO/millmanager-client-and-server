//
//  ViewController.swift
//  M.Agent
//
//  Created by Владимир on 16/03/2019.
//  Copyright © 2019 Wolfram. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var titleNavLabel: DesignableLabel!
    @IBOutlet weak var loginButton: DesignableButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if let login = UserDefaults.standard.string(forKey: "login"), let password = UserDefaults.standard.string(forKey: "password") {
            loginTextField.text = login
            passwordTextField.text = password
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @objc @IBAction func loginAction(_ sender: Any) {
        guard let login = loginTextField.text, login.count > 0,
           let password = passwordTextField.text, password.count > 0 else {
            shake()
            return
        }
        
        loginAnimate()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) { [weak self] in
            request("http://localhost:8080/v1/auth?" + "username=" + login + "&password=" + password).responseJSON{ (response) in
                print(response)
                if let json = response.result.value as? [String: Any],
                    let token = json["token"] as? String {
                        GetToken.token = token
                        print(GetToken.token)
                        UserDefaults.standard.set(login, forKey: "login")
                        UserDefaults.standard.set(password, forKey: "password")
                        self?.performSegue(withIdentifier: "login", sender: nil)
                } else {
                    self?.loginFailAnimate()
                }
            }
        }
    }
    
    func loginFailAnimate() {
        self.loginButton.animation = "squeezeUp"
        self.loginButton.curve = "easeOut"
        self.loginButton.duration = 0.5
        self.loginButton.rotate = 0
        self.loginButton.animate()
    }
    
    func loginAnimate() {
        self.loginButton.animation = "fall"
        self.loginButton.curve = "easeOutQuad"
        self.loginButton.duration = 2
        self.loginButton.rotate = 0
        self.loginButton.animate()
    }
    
    func shake() {
        self.loginButton.animation = "shake"
        self.loginButton.curve = "easeOut"
        self.loginButton.duration = 0.5
        self.loginButton.rotate = 0
        self.loginButton.animate()
    }
    
}

func bill(_ url:String) -> String {
        return "https://10.211.55.9:1500/billmgr?out=JSONdata&auth=" + GetToken.token + "&" + url
}
class GetToken {
    static var token = ""
}


