import Vapor


let servUrl = "https://10.211.55.9:1500/"
let getUrl = servUrl + "billmgr?out=xml&auth=" + token + "&"
let getUrlNoToken = servUrl + "billmgr?out=xml&"
var token = ""

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    
    let userController = UserController()
    let clientController = ClientController()
   // router.get("auth", use: userController.auth)

    router.get("auth", use: userController.auth)
    
    router.group("v1") { (v1) in
        v1.get("auth", use: userController.auth)
        v1.post(ClientRegisterModel.self, at: "createClient", use: clientController.createUser)
    }
}
