//
//  NewClientModel.swift
//  M.Agent
//
//  Created by Владимир on 17/03/2019.
//  Copyright © 2019 Wolfram. All rights reserved.
//

import UIKit
import EVReflection
import Alamofire

struct AccountModel: Codable {
    var email: String?              // Email. Email адрес клиента, также будет использоваться как логин для авторизации в BILLmanager
    var verify_email: String?        // . (Необязательный параметр. Чтобы включить данную опцию используйте значение "on".)
    var project: String?             // Провайдер. Провайдер, к которому будет привязан клиент
    var country: String = "182"      // Страна. Страна клиента
    var state: String?               // Регион. Выберите регион
    var realname: String?            // Контактное лицо. ФИО клиента или контактного лица организации, для которой создается учетная var запись
    var name: String?                // Наименование. Наименование клиента
    var settingurl: String?          // URL настроек. URL получения настроек панелями ISPmanager
    var valid_phone: String?         // Проверенный номер телефона. Проверенный номер телефона клиента
    var try_phone: String?           // Номер телефона. Номер телефона клиента
    var label: String?               // Метка. Служебная информация, доступная в фильтре
    var selfview: String?            // Свое пространство имен. Позволяет подменить указываемые в настройках панели сервера имен
    var passwd: String?              // . Пароль доступа в личный кабинет
    var confirm: String?             // Подтверждение.
    var products_page: String?       // Страница. Страница
    var registration_date: String?   // Дата регистрации. Дата регистрации
    var registration_ip: String?     // IP-адрес регистрации. IP-адрес, с которого произведена регистрация клиента
    var attitude: String?            // Характеристика клиента. Характеристика клиента
    var employee: String?            // Сотрудник. Сотрудник, зарегистрировавший клиента
    var nocalcstat: String?          // . (Необязательный параметр. Чтобы включить данную опцию используйте значение "on".) Услуги var клиента не будут отображаться в статистике
    var note: String?                // Комментарий. Комментарий к клиенту
    var notify: String?              // . (Необязательный параметр. Чтобы включить данную опцию используйте значение "on".) Отправить var клиенту уведомление о регистрации в BILLmanager
    var recovery: String?            // . (Необязательный параметр. Чтобы включить данную опцию используйте значение "on".) Добавить var в уведомление данные для сброса пароля
}
