//
//  ContractListDisplayData.swift
//  M.Agent
//
//  Created by Владимир on 21/03/2019.
//Copyright © 2019 Wolfram. All rights reserved.
//

import Foundation

final class ContractListDisplayData: BaseTableDisplayData {
    var mainTitle: String?
    var addCommand: Command?
}

