//
//  ContractListNavigationController.swift
//  M.Agent
//
//  Created by Владимир on 21/03/2019.
//  Copyright © 2019 Wolfram. All rights reserved.
//

import UIKit

class ContractListNavigationController: UINavigationController {

    override func viewDidLoad() {
        self.viewControllers = [AppModules.ContractList.build().view]
        super.viewDidLoad()
    }

}
