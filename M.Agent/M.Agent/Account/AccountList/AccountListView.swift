//
//  AccountListView.swift
//  M.Agent
//
//  Created by Владимир on 18/03/2019.
//Copyright © 2019 Wolfram. All rights reserved.
//

import UIKit

//MARK: - Public Interface Protocol
protocol AccountListViewInterface {
    var myDisplayData: AccountListDisplayData { get }
    func deselectSelectedRow()
}

//MARK: AccountListView Class
final class AccountListView: BaseTableView {
    @IBOutlet weak var mainLabel: UILabel?
    override func update() {
        super.update()
        mainLabel?.text = displayData.mainTitle
    }
    @IBAction func addButtonAction(_ sender: Any) {
        displayData.addCommand?.perform()
    }
}

//MARK: - Public interface
extension AccountListView: AccountListViewInterface {
    var myDisplayData: AccountListDisplayData {
        return displayData
    }
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension AccountListView {
    var displayData: AccountListDisplayData {
        return _displayData as! AccountListDisplayData
    }
}



