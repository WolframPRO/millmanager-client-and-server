//
//  DisplayDataCommand.swift
//  Viperit
//
//  Created by Alexey Bykov on 1/21/19.
//  Copyright © 2019 Ferran Abelló. All rights reserved.
//

import Foundation
import Viperit

open class DisplayDataCommand {
    public private(set) weak var displayData: DisplayData?
    private var performBlock: (_ command: DisplayDataCommand?) -> Void
    
    public init(displayData: DisplayData, perform performBlockToCall: @escaping (_ command: DisplayDataCommand?) -> Void) {
        self.displayData = displayData
        self.performBlock = performBlockToCall
    }
    
    public func perform() {
        weak var weakSelf = self
        self.performBlock(weakSelf)
    }
    
}
