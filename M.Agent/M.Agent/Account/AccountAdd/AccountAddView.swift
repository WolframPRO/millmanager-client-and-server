//
//  AccountAddView.swift
//  M.Agent
//
//  Created by Владимир on 18/03/2019.
//Copyright © 2019 Wolfram. All rights reserved.
//

import UIKit

//MARK: - Public Interface Protocol
protocol AccountAddViewInterface {
    var myDisplayData: AccountAddDisplayData { get }
    func deselectSelectedRow()
    func errorCreate()
}

//MARK: AccountAddView Class
final class AccountAddView: BaseTableView {
    @IBOutlet weak var mainLabel: UILabel?
    override func update() {
        super.update()
        mainLabel?.text = displayData.mainTitle
    }
    @IBAction func doneButtonAction(_ sender: Any) {
        displayData.doneCommand?.perform()
    }
    
}

//MARK: - Public interface
extension AccountAddView: AccountAddViewInterface {
    func errorCreate() {
        DispatchQueue.main.async {
            let cancelAction = UIAlertAction(title: "Ок", style: .default) { (_) -> Void in
            }
            
            let alert = UIAlertController(title: "Ошибка", message: "При создании пользователя возникла ошибка, проверьте все поля", preferredStyle: .alert)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    var myDisplayData: AccountAddDisplayData {
        return displayData
    }
    
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension AccountAddView {
    var displayData: AccountAddDisplayData {
        return _displayData as! AccountAddDisplayData
    }
}



