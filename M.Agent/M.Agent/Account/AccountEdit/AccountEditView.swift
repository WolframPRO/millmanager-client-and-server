//
//  AccountEditView.swift
//  M.Agent
//
//  Created by Владимир on 18/03/2019.
//Copyright © 2019 Wolfram. All rights reserved.
//

import UIKit

//MARK: - Public Interface Protocol
protocol AccountEditViewInterface {
    var myDisplayData: AccountEditDisplayData { get }
    func deselectSelectedRow()
    func errorDelete()
}

//MARK: AccountEditView Class
final class AccountEditView: BaseTableView {
    @IBOutlet weak var mainLabel: UILabel?
    @IBOutlet weak var trashButton: UIBarButtonItem!
    
    
    override func update() {
        super.update()
        mainLabel?.text = displayData.mainTitle
        self.trashButton.isEnabled = displayData.deleteCommand != nil
    }
    @IBAction func trashButtonAction(_ sender: Any) {
        if let command = displayData.deleteCommand {
            command.perform()
        }
    }
    
    
}

//MARK: - Public interface
extension AccountEditView: AccountEditViewInterface {
    var myDisplayData: AccountEditDisplayData {
        return displayData
    }
    
    func errorDelete() {
        DispatchQueue.main.async {
            let cancelAction = UIAlertAction(title: "Ок", style: .default) { (_) -> Void in
            }
            let alert = UIAlertController(title: "Ошибка", message: "При удалении клиента возникла ошибка", preferredStyle: .alert)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension AccountEditView {
    var displayData: AccountEditDisplayData {
        return _displayData as! AccountEditDisplayData
    }
}



