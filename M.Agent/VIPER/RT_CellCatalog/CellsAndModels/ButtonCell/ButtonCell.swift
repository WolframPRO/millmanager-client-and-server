//
//  ButtonCellTableViewCell.swift
//  RT_CellCatalogPod
//
//  Created by Вова Петров on 18/01/2019.
//  Copyright © 2019 DartIT. All rights reserved.
//

import UIKit

final class ButtonCell: SetModelBaseCell {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func update() {
        super.update()
        guard let model = self.model as? BasicCells.ButtonCellModel else {
            return
        }
        
        label.text = model.labelText
        
        if let activityIndicator = self.activityIndicator {
            if model.activityIndicatorAnimating {
                activityIndicator.startAnimating()
            } else {
                activityIndicator.stopAnimating()
            }
        }
        self.accessoryType = .none
    }    
}
