//
//  UITableView+TextHeight.m
//  OnLime
//
//  Created by bykov-av on 06/02/2017.
//  Copyright © 2017 Dart-IT. All rights reserved.
//

#import "UITableView+RowHeight.h"


@implementation UITableView (RowHeight)


- (CGFloat)heightOfText:(NSString*)text withMargin:(CGFloat)margin ofFontSize:(CGFloat)fontSize semiBold:(BOOL)semiBold {

    CGRect rect = [text boundingRectWithSize:CGSizeMake(self.bounds.size.width - 30 - margin, CGFLOAT_MAX)
                                     options: NSStringDrawingUsesLineFragmentOrigin//|NSStringDrawingUsesFontLeading
                                  attributes:@{NSFontAttributeName:semiBold ? [UIFont boldSystemFontOfSize:fontSize] :[UIFont systemFontOfSize:fontSize]}
                                     context:nil];
    return rect.size.height;
}

- (CGFloat)heightOfText:(NSString*)text withMargin:(CGFloat)margin ofFontSize:(CGFloat)fontSize semiBold:(BOOL)semiBold truncatedByNumberOfLines:(NSUInteger)numberOfLines {
    CGRect rect = [text boundingRectWithSize:CGSizeMake(self.bounds.size.width - 30 - margin, fontSize * (numberOfLines + 0.5))
                                     options: NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingTruncatesLastVisibleLine
                                  attributes:@{NSFontAttributeName:semiBold ? [UIFont boldSystemFontOfSize:fontSize] :[UIFont systemFontOfSize:fontSize]}
                                     context:nil];
    return rect.size.height;
}

- (CGFloat)widthOfText:(NSString*)text withMargin:(CGFloat)margin ofFontSize:(CGFloat)fontSize semiBold:(BOOL)semiBold {
    
    CGRect rect = [text boundingRectWithSize:CGSizeMake(self.bounds.size.width - 30 - margin, CGFLOAT_MAX)
                                     options: NSStringDrawingUsesLineFragmentOrigin//|NSStringDrawingUsesFontLeading
                                  attributes:@{NSFontAttributeName:semiBold ? [UIFont boldSystemFontOfSize:fontSize] :[UIFont systemFontOfSize:fontSize]}
                                     context:nil];
    return rect.size.width;
}


- (CGFloat)heightOfText:(NSString*)text withMargin:(CGFloat)margin ofFontSize:(CGFloat)fontSize {
    
    return [self heightOfText:text withMargin:margin ofFontSize:fontSize semiBold:NO];
    
}


- (CGFloat)heightOfText:(NSString*)text withMargin:(CGFloat)margin ofFontSize:(CGFloat)fontSize truncatedByNumberOfLines:(NSUInteger)numberOfLines {
    
    return [self heightOfText:text withMargin:margin ofFontSize:fontSize semiBold:NO truncatedByNumberOfLines:numberOfLines];
    
}


//

- (CGFloat)heightOfNormalText:(NSString*)text withMargin:(CGFloat)margin {
    return [self heightOfText:text withMargin:margin ofFontSize:16.0];
}
- (CGFloat)heightOfNormalText:(NSString*)text {
    return [self heightOfNormalText:text withMargin:0];
}
- (CGFloat)widthOfNormalText:(NSString *)text {
    return [self widthOfText:text withMargin:0 ofFontSize:16.0 semiBold:NO];
}

//

- (CGFloat)heightOfPriceText:(NSString*)text withMargin:(CGFloat)margin {
    return [self heightOfText:text withMargin:0 ofFontSize:17.0];
}
- (CGFloat)heightOfPricelText:(NSString *)text {
    return [self heightOfPriceText:text withMargin:0];
}
- (CGFloat)widthOfPricelText:(NSString *)text {
    return [self widthOfText:text withMargin:0 ofFontSize:17.0 semiBold:NO];
}

//

- (CGFloat)heightOfMenuText:(NSString*)text withMargin:(CGFloat)margin {
    return [self heightOfText:text withMargin:margin ofFontSize:17.0];
}
- (CGFloat)heightOfMenuText:(NSString*)text withMargin:(CGFloat)margin truncatedByNumberOfLines:(NSUInteger)numberOfLines {
    return [self heightOfText:text withMargin:margin ofFontSize:17.0 truncatedByNumberOfLines:numberOfLines];
}
- (CGFloat)widthOfMenuText:(NSString *)text {
    return [self widthOfText:text withMargin:0 ofFontSize:17.0 semiBold:NO];
}


- (CGFloat)heightOfMenuText:(NSString*)text {
    return [self heightOfMenuText:text withMargin:0];
}
- (CGFloat)heightOfSemiboldMenuText:(NSString*)text withMargin:(CGFloat)margin {
    return [self heightOfText:text withMargin:margin ofFontSize:17.0 semiBold:YES];
}
- (CGFloat)heightOfSemiboldMenuText:(NSString*)text withMargin:(CGFloat)margin truncatedByNumberOfLines:(NSUInteger)numberOfLines {
    return [self heightOfText:text withMargin:margin ofFontSize:17.0 semiBold:YES truncatedByNumberOfLines:numberOfLines];
}

- (CGFloat)heightOfSemiboldMenuText:(NSString*)text {
    return [self heightOfSemiboldMenuText:text withMargin:0];
}

//

- (CGFloat)heightOfSmallText:(NSString*)text withMargin:(CGFloat)margin {
    return [self heightOfText:text withMargin:margin ofFontSize:14.0];
}
- (CGFloat)heightOfSmallText:(NSString*)text withMargin:(CGFloat)margin truncatedByNumberOfLines:(NSUInteger)numberOfLines {
    return [self heightOfText:text withMargin:margin ofFontSize:14.0 truncatedByNumberOfLines:numberOfLines];
}
- (CGFloat)heightOfSmallText:(NSString*)text {
    return [self heightOfSmallText:text withMargin:0];
}
- (CGFloat)widthOfSmallText:(NSString *)text {
    return [self widthOfText:text withMargin:0 ofFontSize:14.0 semiBold:NO];
}




@end
