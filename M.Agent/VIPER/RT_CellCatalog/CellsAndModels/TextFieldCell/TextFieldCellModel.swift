//
//  TwoLabelCellModel.swift
//  RT_CellCatalogPod
//
//  Created by Вова Петров on 26/12/2018.
//  Copyright © 2018 DartIT. All rights reserved.
//

import UIKit

public enum TextFieldCellModelType: CellModelTypeProtocol {
    case standart
    public var nibNameAndReuseIdentifier: (String, String) {
        switch self {
            case .standart:
                return ("TextFieldCell", "TextFieldCell")
        }
    }
}

public extension BasicCells {
    public final class TextFieldCellModel: BaseCellModel {

        public var labelText: String
        public var fieldText: String
        public var fieldPlaceholderText: String
        public var inputType: UIKeyboardType
        
        public var isPasteEnabled: Bool = true
        public var isSelectEnabled: Bool = true
        public var isSelectAllEnabled: Bool = true
        public var isCopyEnabled: Bool = true
        public var isCutEnabled: Bool = true
        public var isDeleteEnabled: Bool = true
        
        public var textFieldDelegate: UITextFieldDelegate?
        
        public init(labelText: String, fieldText: String, fieldPlaceholderText: String, inputType: UIKeyboardType = .default, type: TextFieldCellModelType = .standart, style: CellModelStyle? = nil) {
            self.labelText = labelText
            self.fieldText = fieldText
            self.fieldPlaceholderText = fieldPlaceholderText
            self.inputType = inputType
            
            super.init(reuseIdentifier: type.nibNameAndReuseIdentifier.1)
            initStyle(style)
            initTypes(with: TextFieldCellModelType.allCases)
        }
    }
}
