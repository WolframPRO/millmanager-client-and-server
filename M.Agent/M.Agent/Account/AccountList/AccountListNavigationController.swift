//
//  AccountListNavigationControllerViewController.swift
//  M.Agent
//
//  Created by Владимир on 18/03/2019.
//  Copyright © 2019 Wolfram. All rights reserved.
//

import UIKit

class AccountListNavigationController: UINavigationController {

    override func viewDidLoad() {
        self.viewControllers = [AppModules.AccountList.build().view]
        super.viewDidLoad()
    }
}
