//
//  CellModelStyles.swift
//  RT_CellCatalogPod
//
//  Created by Alexey Bykov on 1/31/19.
//  Copyright © 2019 Rostelecom. All rights reserved.
//

import Foundation

public enum CellModelStyle: String, CellModelStyleBuilderProtocol {
    
    case mlk
    case onlime
    case agent
    
    case agentReddish
    
    func specialStyleClassAndItsBaseStyle() -> (CellModelStyleBase.Type, CellModelStyle)? {
        switch self {
        case .agentReddish:
            return (ButtonCellModelSpecialStyle.self, .agent)
        default:
            return nil
        }
    }
    
    static private var _defaultStyle: CellModelStyle = .mlk
    static var defaultStyle: CellModelStyle {
        return _defaultStyle
    }
    
    public static func setDefaultStyle(defaultStyle: CellModelStyle) {
        _defaultStyle = defaultStyle
    }
}
