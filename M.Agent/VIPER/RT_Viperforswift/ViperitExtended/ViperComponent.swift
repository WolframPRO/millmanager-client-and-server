//
//  ViperComponents.swift
//  Viperit
//
//  Created by Ferran on 11/09/2016.
//

import Foundation

internal enum ViperComponent: String {
    case view
    case interactor
    case presenter
    case router
    case displayData
}
