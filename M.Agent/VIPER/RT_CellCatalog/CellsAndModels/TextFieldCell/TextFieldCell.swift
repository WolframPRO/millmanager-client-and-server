//
//  TextFieldCell.swift
//  RT_CellCatalogPod
//
//  Created by Вова Петров on 07/02/2019.
//  Copyright © 2019 Rostelecom. All rights reserved.
//

import UIKit

final class TextFieldCell: SetModelBaseCell {

    //MARK: - Outlets
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: CustomTextField!
    @IBOutlet weak var textFieldLeftContraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func update() {
        super.update()
        guard let model = self.model as? BasicCells.TextFieldCellModel else {
            return
        }
        label.text = model.labelText
        textField.text = model.fieldText
        textField.placeholder = model.fieldPlaceholderText
        
        textField.isPasteEnabled     = model.isPasteEnabled
        textField.isSelectEnabled    = model.isSelectEnabled
        textField.isSelectAllEnabled = model.isSelectAllEnabled
        textField.isCopyEnabled      = model.isCopyEnabled
        textField.isCutEnabled       = model.isCutEnabled
        textField.isDeleteEnabled    = model.isDeleteEnabled
        
        textField.delegate = model.textFieldDelegate
        
        label(hide: model.labelText.count == 0)
    }
    
    fileprivate func label(hide: Bool){
        if let leftConstraint = self.textFieldLeftContraint {
            leftConstraint.isActive = !hide
            self.contentView.layoutIfNeeded()
        }
    }
    
    override func haveCommandAccessoryType() -> UITableViewCell.AccessoryType {
        return .disclosureIndicator
    }

}
