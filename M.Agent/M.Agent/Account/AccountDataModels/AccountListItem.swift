//
//  AccountListItem.swift
//  M.Agent
//
//  Created by Владимир on 18/03/2019.
//  Copyright © 2019 Wolfram. All rights reserved.
//

import UIKit

struct AccountListItem: Codable {
    
    var tz_registration_date: String //":"2019-03-14 23:02:01",
    var balance: String //":"0.00 RUB",
    var abuse_rate: String //":"0",
    var country: String //":"182",
    var need_manual_vetting: String //":"off",
    var need_activation: String //":"off",
    var conversion_label_userid: String //":"off",
    var nocalcstat: String //":"off",
    var ignoreabuserate: String //":"off",
    var ignoreitemmax: String //":"off",
    var allowsuspenditem: String //":"off",
    var allowdeleteitem: String //":"off",
    var attitude: String //":"0",
    var note: String? //":"make client",
    var registration_date: String //":"2019-03-14 23:02:01",
    var level: String //":"16",
    var name: String //":"ivanov ivan (ivanovivan@mail.ru)",
    var id: String //":"3"
    
}
