//
//  String+.swift
//  Viperit
//
//

import Foundation

public extension String {
    var first: String {
        return String(prefix(1))
    }
    var uppercasedFirst: String {
        return first.uppercased() + String(dropFirst())
    }
    
    func stripHTML() -> String {
        var result = self
        while let range = result.range(of: "<[^>]+>",
                                       options: String.CompareOptions.regularExpression,
                                       range: nil, locale: nil),
            !range.isEmpty
        {
            result = result.replacingCharacters(in: range, with: "")
        }
        return result
    }
    
    func hasHTML() -> Bool {
        return self.stripHTML() != self
    }

}
