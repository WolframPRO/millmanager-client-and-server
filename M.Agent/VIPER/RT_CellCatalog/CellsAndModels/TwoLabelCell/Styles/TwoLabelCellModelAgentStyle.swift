//
//  TwoLabelCellModelAgentStyle.swift
//  RT_CellCatalogPod
//
//  Created by Alexey Bykov on 2/1/19.
//  Copyright © 2019 Rostelecom. All rights reserved.
//

import Foundation
import UIKit

public final class TwoLabelCellModelAgentStyle: CellModelStyleBase {
    required public init(with styleToBeDecorated: CellModelStyleProtocol?) {
        super.init(with: styleToBeDecorated)
    }
    override public func imbue() {
        super.imbue()
        // my imbue
        if  let model = self.model as? BasicCells.TwoLabelCellModel,
            let cell = model.cell as? TwoLabelCell
        {
            switch model.type {
            case .horizontal:
                cell.oneLabel.textColor = model.isReadOnly ? UIColor.agent.text.readOnly : UIColor.agent.text.common
                cell.twoLabel.textColor = UIColor.agent.text.readOnly
            case .vertical:
                cell.oneLabel.textColor = UIColor.agent.text.readOnly
                cell.twoLabel.textColor = model.isReadOnly ? UIColor.agent.text.readOnly : UIColor.agent.text.common
            case .value:
                cell.oneLabel.textColor = model.isReadOnly ? UIColor.agent.text.readOnly : UIColor.agent.text.common
                cell.twoLabel.textColor = UIColor.agent.text.readOnly
            }
        }
    }
    
}
