//
//  AppModules.swift
//  M.Agent
//
//  Created by Владимир on 16/03/2019.
//  Copyright © 2019 Wolfram. All rights reserved.
//

import UIKit

enum AppModules: String, ViperitExtendedModule {
    case AccountList
    case AccountEdit
    case AccountAdd
    case ContractList
}
