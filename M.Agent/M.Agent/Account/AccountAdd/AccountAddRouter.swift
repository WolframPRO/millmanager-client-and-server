//
//  AccountAddRouter.swift
//  M.Agent
//
//  Created by Владимир on 18/03/2019.
//Copyright © 2019 Wolfram. All rights reserved.
//

import Foundation


final class AccountAddRouter: RouterExtended {

}

extension AccountAddRouter: FeedbackProtocol {
    var feedbackID: String {
        return "expectedFeedbackInRouter"
    }
    func receiveFeedbackFrom(_ presenter: PresenterAPIProtocol, forFeedbackID: String) {
        /*
         if let _ = presenter as? SecondPresenterAPI {
         */
        self.presenter.refreshTableState()
        /*
         }
         */
        
    }
}


// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension AccountAddRouter {
    var presenter: AccountAddPresenter {
        return _presenter as! AccountAddPresenter
    }
}


