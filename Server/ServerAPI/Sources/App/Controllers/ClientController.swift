//
//  ClientController.swift
//  App
//
//  Created by Владимир on 14/03/2019.
//

import Crypto
import Vapor
import FluentSQLite
import Async

final class ClientController {

    func createUser(_ req: Request, clientModel: ClientRegisterModel) throws -> Future<AuthResponse> {
        
        let client = try req.make(Client.self)
        return client.post(getUrl + "func=account.edit&country=182&sok=ok" + clientModel.createUrlHeaders()).map { response in
            
            let xml = XML.parse(response.http.body.data!)
            if let id = xml["doc","auth"].attributes["id"] {
                return AuthResponse(token: id, isError: false)
            } else {
                return AuthResponse(token: nil, isError: true)
            }
        }
    }
    
}


struct ClientRegisterModel: Content {
    var email: String
    var realname: String
    var note: String
    var password: String //passwd
    var notify: String = "off"
    
    func createUrlHeaders() -> String{
        return "&email="    + email     +
               "&notify="   + notify    +
               "&realname=" + realname  +
               "&note="     + note      +
               "&passwd="   + password
    }
}
