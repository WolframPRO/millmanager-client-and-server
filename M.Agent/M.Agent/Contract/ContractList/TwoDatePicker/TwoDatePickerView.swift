//
//  TwoDatePickerView.swift
//  MobileAgent
//
//  Created by Вова Петров on 31/01/2019.
//  Copyright © 2019 DartIT. All rights reserved.
//

import UIKit
import SnapKit

class TwoDatePickerView: UIView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet private weak var visualEffectView: UIVisualEffectView!
    @IBOutlet private weak var textField: UITextField!
    @IBOutlet private weak var topConstaint: NSLayoutConstraint!
    private var completion: ((String) -> ())?
    var picker: UIPickerView!
    var accounts: [ProfileAccount]!
    
    private override init(frame: CGRect) {
        super.init(frame: frame)
        let view = Bundle.main.loadNibNamed("TwoDatePickerView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    ///Этот метод очень тяжелый, рекомендуется использовать его заранее, например в ViewWillAppear
    init(accounts: [ProfileAccount], completion: @escaping (String) -> ()) {
        self.init()
        self.accounts = accounts
        self.configure(completion: completion)
    }
    
    private func configure(completion: @escaping (String) -> ()) {
        self.picker = UIPickerView()
        self.picker.delegate = self
        self.picker.dataSource = self
        textField.inputView = picker
        
        self.completion = completion
        
        visualEffectView.contentView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(closeSelf)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }    
    
    ///Не рекомендуется создавать класс и использовать метод showFrom в одном месте, это может замедлить анимацию. Используйте метод init класса при ViewWillAppear контроллера
    public func showFrom(view: UIView) {
        view.addSubview(self)
        self.snp.makeConstraints { (make) in
            make.bottom.equalTo(view.snp.bottom)
            make.left.equalTo(view.snp.left)
            make.right.equalTo(view.snp.right)
            make.top.equalTo(view.snp.topMargin)
        }
        view.layoutIfNeeded()
        visualEffectView.effect = nil
        
        self.topConstaint.constant = -120

        var blurEffect: UIVisualEffect? = nil
        if #available(iOS 10.0, *) {
            blurEffect = UIBlurEffect(style: .light)
        }

        UIView.animate(withDuration: 0.25) {
            if #available(iOS 10.0, *) {
                self.visualEffectView.effect = blurEffect
            } else {
                self.visualEffectView.backgroundColor = .groupTableViewBackground
            }
            self.layoutIfNeeded()
        }
        
        textField.becomeFirstResponder()
    }
    
    @objc public func closeSelf() {
        topConstaint.constant = -290
        self.endEditing(true)
        UIView.animate(withDuration: 0.25, animations: {
            if #available(iOS 10.0, *) {
                self.visualEffectView.effect = nil
            } else {
                self.visualEffectView.backgroundColor = .clear
            }
            self.layoutIfNeeded()
        }) { (_) in
            self.removeFromSuperview()
        }
        if let completion = self.completion {
            completion(self.textField.text ?? "")
        }
    }
   
    @IBAction func doneButtonAction(_ sender: Any) {
        self.closeSelf()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return accounts.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "id: " + accounts[row].id + " - " + accounts[row].account
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.textField.text = accounts[row].id
    }
}
