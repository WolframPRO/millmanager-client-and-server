//
//  RouterExtended.swift
//  MyExample
//
//  Created by Alexey Bykov on 12/5/18.
//

import Foundation
import Viperit

open class RouterExtended: Router {
    
    open func present(from: UIViewController, embedInNavController: Bool = false, setupData: Any? = nil) {
        if let data = setupData {
            _presenter.setupView(data: data)
        }
        let view: UIViewController = embedInNavController ? embedInNavigationController() : _view
        if  embedInNavController,
            let navigationViewController = view as? UINavigationController
        {
            navigationViewController.navigationBar.isTranslucent = false
        }
        from.present(view, animated: true, completion: nil)
    }
    
    open func showDetail(from: UIViewController, embedInNavController: Bool = false, setupData: Any? = nil) {
        if let data = setupData {
            _presenter.setupView(data: data)
        }
        let view: UIViewController = embedInNavController ? embedInNavigationController() : _view
        if  embedInNavController,
            let navigationViewController = view as? UINavigationController
        {
            navigationViewController.navigationBar.isTranslucent = false
        }
        from.showDetailViewController(view, sender: nil)
    }
    
    open func close() {
        _view.dismiss(animated: true, completion: nil)
    }
    open func back() {
        _view.navigationController?.popViewController(animated: true)
    }
}

extension RouterExtended {
    public func setupAndShow<T: PresenterSetuperProtocol>(module moduleEnum: ViperitExtendedModule, with setupData: T.PresenterSetupStruct, for presenterOfClass: T.Type)
    {
        let module = moduleEnum.build(bundle: Bundle.main, deviceType: nil)
        module.router.show(from: _view,
                           embedInNavController: false,
                           setupData: setupData)
    }
    
    public func setupAndPresent<T: PresenterSetuperProtocol>(module moduleEnum: ViperitExtendedModule, with setupData: T.PresenterSetupStruct, for presenterOfClass: T.Type)
    {
        let module = moduleEnum.build(bundle: Bundle.main, deviceType: nil)
        module.router.present(from: _view,
                              embedInNavController: true,
                              setupData: setupData)
    }
}

