//
//  BaseTableDisplayData.swift
//  Example
//
//  Created by Alexey Bykov on 12/4/18.
//

import Foundation
import Viperit

open class BaseTableDisplayData: DisplayData, TableDisplayData {
    var sections: [TableSectionViewModel]?
    
    public func setupSections(_ sections: [TableSectionViewModel]?) {
        self.sections = sections
    }
}
