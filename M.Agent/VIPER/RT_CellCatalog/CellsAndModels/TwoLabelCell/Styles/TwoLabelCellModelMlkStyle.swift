//
//  TwoLabelCellModelMlkStyle.swift
//  RT_CellCatalogPod
//
//  Created by Alexey Bykov on 2/1/19.
//  Copyright © 2019 Rostelecom. All rights reserved.
//

import Foundation
import UIKit

public final class TwoLabelCellModelMlkStyle: CellModelStyleBase {
    required public init(with styleToBeDecorated: CellModelStyleProtocol?) {
        super.init(with: styleToBeDecorated)
    }
    override public func imbue() {
        super.imbue()
        // my imbue
        if  let model = self.model,
            let cell = model.cell as? TwoLabelCell {
            cell.oneLabel.textColor = UIColor.black
            cell.twoLabel.textColor = model.isReadOnly ? UIColor.gray : UIColor.red
        }
    }
    
}
