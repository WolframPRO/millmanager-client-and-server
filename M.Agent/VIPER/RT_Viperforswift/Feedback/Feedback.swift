//
//  Feedback.swift
//  MyExample
//
//  Created by Alexey Bykov on 12/5/18.
//

import Foundation
import Viperit

public protocol PresenterSetupProtocol {
}

public protocol PresenterSetuperProtocol {
    associatedtype PresenterSetupStruct: PresenterSetupProtocol
}

public protocol PresenterAPIProtocol {
}



public protocol FeedbackProtocol {
    var feedbackID: String { get }
    func receiveFeedbackFrom(_ presenter: PresenterAPIProtocol, forFeedbackID: String)
}



public struct FeedbackHolder: FeedbackProtocol {
    
    public func receiveFeedbackFrom(_ presenter: PresenterAPIProtocol, forFeedbackID: String) {
        feedbackBlock(presenter, forFeedbackID)
    }
    
    public let feedbackID: String
    private let feedbackBlock: (_ presenter: PresenterAPIProtocol, _ feedbackID: String) -> Void
    
    public init(feedbackID: String, feedbackBlock: @escaping (_ presenter: PresenterAPIProtocol, _ feedbackID: String) -> Void) {
        self.feedbackID = feedbackID
        self.feedbackBlock = feedbackBlock
    }
}
