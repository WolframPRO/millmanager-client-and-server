//
//  UserController.swift
//  App
//
//  Created by Владимир on 13/03/2019.
//

import Crypto
import Vapor
import FluentSQLite
import Async

final class UserController {

    func auth(_ req: Request) throws -> Future<AuthResponse> {
        
        guard let username = req.query[String.self, at: "username"] else {
            throw Abort(.badRequest)
        }
        guard let password = req.query[String.self, at: "password"] else {
            throw Abort(.badRequest)
        }
        
        let client = try req.make(Client.self)
        return client.get(getUrlNoToken + "func=auth&username=" + username + "&password=" + password).map { response in
            let xml = XML.parse(response.http.body.data!)
            
            if let id = xml["doc","auth"].attributes["id"] {
                token = id
                return AuthResponse(token: id, isError: false)
            } else {
                return AuthResponse(token: nil, isError: true)
            }
        }
    }
    
}

struct AuthResponse: Content {
    
    var token: String?
    var isError: Bool
    
}
