//
//  ContractListItem.swift
//  M.Agent
//
//  Created by Владимир on 21/03/2019.
//  Copyright © 2019 Wolfram. All rights reserved.
//

import UIKit

struct ContractListItem: Codable {
    var contract_name: String           //КрутойДоговор",
    var company_name: String            //Крутая компания",
    var client_name: String             //Платльщик007",
    var contracttype: String            //0",
    var status: String                  //1",
    var companycontract: String         //1",
    var profile: String                 //3",
    var signdate: String                //2019-03-21",
    var number: String                  //3",
    var id: String                      //1",
    var signdate_l: String              //21 марта 2019 г.",
    var signdate_s: String              //21 мар 2019 г.",
    var signdate_locale_time: String    //00:00"
}
