//
//  AccountEditInteractor.swift
//  M.Agent
//
//  Created by Владимир on 18/03/2019.
//Copyright © 2019 Wolfram. All rights reserved.
//

import Foundation
import CodableFirebase
import Alamofire

final class AccountEditInteractor: BaseTableInteractor {
    
    override func load() {
        if presenter.elid.count > 0 {
            manager.request(bill("func=account.edit&elid=" + presenter.elid), method: .get).responseJSON {(response) in
                guard self != nil else {
                    return
                }
                do {
                    self.presenter.account = try FirebaseDecoder().decode(AccountShowModel.self, from: response.result.value!)
                } catch{}
                super.load()
            }
        } else {
            super.load()
        }
    }
    //elid
    func deleteClient(completion: @escaping (Bool) -> Void) {
        guard let elid = presenter.account?.elid else {
            completion(false)
            return
        }
        manager.request(bill("func=account.delete"), method: .get, parameters: ["elid":elid]).responseJSON { (response) in
            print(response)
            completion(true)
        }
    }
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension AccountEditInteractor {
    var presenter: AccountEditPresenter {
        return _presenter as! AccountEditPresenter
    }
}


