//
//  BaseCellModelOnlimeStyle.swift
//  RT_CellCatalogPod
//
//  Created by Alexey Bykov on 2/1/19.
//  Copyright © 2019 Rostelecom. All rights reserved.
//

import Foundation
import UIKit

public final class BaseCellModelOnlimeStyle: CellModelStyleBase {
    required public init(with styleToBeDecorated: CellModelStyleProtocol?) {
        super.init(with: styleToBeDecorated)
    }
    override public func imbue() {
        super.imbue()
        // my imbue
        if let cell = self.model?.cell as? UITableViewCell {
            cell.contentView.backgroundColor = UIColor.gray.withAlphaComponent(0.1)
        }
    }
}

public extension BaseCellModelOnlimeStyle {
    static var onlimeColor: UIColor {
        return UIColor(red: 146.0 / 255.0 , green: 212.0 / 255.0, blue: 0 / 255.0, alpha: 1)
    }
    static var buttonTitleColor: UIColor {
        return onlimeColor
    }
}
