//
//  AccountAddPresenter.swift
//  M.Agent
//
//  Created by Владимир on 18/03/2019.
//Copyright © 2019 Wolfram. All rights reserved.
//

import Foundation

struct AccountAddPresenterSetup: PresenterSetupProtocol {
    let okFeedback: FeedbackProtocol
    let closeFeedback: FeedbackProtocol
}

protocol AccountAddPresenterAPI: PresenterAPIProtocol {
    func showSecondModule()
    func refreshTableState()
}


final class AccountAddPresenter: BaseTablePresenter, PresenterSetuperProtocol {
    
    typealias PresenterSetupStruct = AccountAddPresenterSetup
    
    var account: AccountModel = AccountModel()
    var passwordConfirmCorrect: Bool = true
    
    fileprivate var closeFeedback: FeedbackProtocol?
    fileprivate var okFeedback: FeedbackProtocol?
    
    override func setupView(data: Any) {
        if let setup = data as? AccountAddPresenterSetup
        {
            okFeedback = setup.okFeedback
            closeFeedback = setup.closeFeedback
        }
    }
    
    override func buildSections() -> [TableSectionViewModel] {
        // common views' data setup
        
        view.myDisplayData.mainTitle = "Регистрация клиента"
        
        view.myDisplayData.doneCommand = Command(command: {[weak self] in
            guard self?.passwordConfirmCorrect ?? false else {
                self?.view.errorCreate()
                return
            }
            self?.interactor.createClient {success in
                if success {
                    self?.okFeedback?.receiveFeedbackFrom(self ?? AccountAddPresenter(),
                                                          forFeedbackID: "addComplete")
                    self?.router.back()
                } else {
                    self?.view.errorCreate()
                }
            }
        })
        
        // table view data setup
        let s1 = WFTableSectionModel.init()
        let emailModel = BasicCells.TextFieldCellModel(labelText: "Email", fieldText: account.email ?? "", fieldPlaceholderText: "Введите Email")
        emailModel.inputType = .emailAddress
        emailModel.textFieldDelegate = TextFieldCellDelegate(changeCommand: {[weak self] (text) -> Bool in
            self?.account.email = text
            return true
        })
        let verifyEmailModel = BasicCells.SwitchCellModel(labelText: "Нужно ли проверять Email", switchIsOn: account.verify_email == "on" ? true : false)
        verifyEmailModel.switchCommand = TableSectionViewModelCommand(sectionViewModel: s1, perform: {[weak self] (_) in
            self?.account.verify_email = verifyEmailModel.switchIsOn ?? false ? "on" : nil
        })
        
        let projectModel = BasicCells.TextFieldCellModel(labelText: "Провайдер", fieldText: account.project ?? "", fieldPlaceholderText: "Ростелеком")
        projectModel.textFieldDelegate = TextFieldCellDelegate(changeCommand: {[weak self] (text) -> Bool in
            self?.account.project = text
            return true
        })
        
        let nameModel = BasicCells.TextFieldCellModel(labelText: "Контактное лицо", fieldText: account.realname ?? "", fieldPlaceholderText: "Иванов И.И.")
        nameModel.textFieldDelegate = TextFieldCellDelegate(changeCommand: {[weak self] (text) -> Bool in
            self?.account.realname = text
            return true
        })
        let phoneModel = BasicCells.TextFieldCellModel(labelText: "Телефон", fieldText: account.valid_phone ?? "+7", fieldPlaceholderText: "+7")
        phoneModel.inputType = .phonePad
        phoneModel.textFieldDelegate = TextFieldCellDelegate(changeCommand: {[weak self] (text) -> Bool in
            self?.account.valid_phone = text
            return true
        })
        let passwordModel = BasicCells.TextFieldCellModel(labelText: "Пароль", fieldText: account.passwd ?? "", fieldPlaceholderText: "")
        passwordModel.textFieldDelegate = TextFieldCellDelegate(changeCommand: {[weak self] (text) -> Bool in
            self?.account.passwd = text
            return true
        })
        let confirmPasswordModel = BasicCells.TextFieldCellModel(labelText: "Подтверждение", fieldText: "", fieldPlaceholderText: "")
        confirmPasswordModel.textFieldDelegate = TextFieldCellDelegate(changeCommand: {[weak self] (text) -> Bool in
            self?.passwordConfirmCorrect = self?.account.passwd == text
            return true
        })
        let commentLabel = BasicCells.TwoLabelCellModel(oneLabelText: "Описание", twoLabelText: "")
        let commentModel = BasicCells.TextFieldCellModel(labelText: "", fieldText: account.note ?? "", fieldPlaceholderText: "Введите описание")
        commentModel.textFieldDelegate = TextFieldCellDelegate(changeCommand: {[weak self] (text) -> Bool in
            self?.account.note = text
            return true
        })
        
        let notifyModel = BasicCells.SwitchCellModel(labelText: "Уведомить клиента", switchIsOn: account.notify == "on" ? true : false)
        notifyModel.switchCommand = TableSectionViewModelCommand(sectionViewModel: s1, perform: {[weak self] (_) in
            self?.account.notify = verifyEmailModel.switchIsOn ?? false ? "on" : nil
        })
        
        s1.rows = [emailModel, verifyEmailModel, projectModel, nameModel, phoneModel, passwordModel, confirmPasswordModel, commentLabel, commentModel, notifyModel]
        return [s1]
    }
    
    override func viewIsAboutToDisappear() {
        if let feedBack = closeFeedback {
            feedBack.receiveFeedbackFrom(self, forFeedbackID: feedBack.feedbackID)
        }
    }
}

// MARK: - Local API
extension AccountAddPresenter: AccountAddPresenterAPI {
    func showSecondModule() {
        /*
         router.showSecondModule(with:
         SecondPresenterSetup(someImportantParam: "someImportantParam",
         okFeedback: FeedbackHolder(feedbackID: "expectedOKFeedback") { (presenter, forFeedbackID) in
         
         },
         closeFeedback: router))
         */
    }
    func refreshTableState() {
        view.deselectSelectedRow()
    }
}


// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension AccountAddPresenter {
    var view: AccountAddViewInterface {
        return _view as! AccountAddViewInterface
    }
    var interactor: AccountAddInteractor {
        return _interactor as! AccountAddInteractor
    }
    var router: AccountAddRouter {
        return _router as! AccountAddRouter
    }
}


