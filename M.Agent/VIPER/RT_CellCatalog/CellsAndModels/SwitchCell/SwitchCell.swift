//
//  SwitchCell.swift
//  RT_CellCatalogPod
//
//  Created by Вова Петров on 26/12/2018.
//  Copyright © 2018 DartIT. All rights reserved.
//

import UIKit

final class SwitchCell: SetModelBaseCell {

    @IBOutlet weak var switchView: UISwitch!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func update() {
        super.update()
        guard let model = self.model as? BasicCells.SwitchCellModel else {
            return
        }
        if let switchIsOn = model.switchIsOn { //Если в модели нет состояния свича, нет смысла его отображать
            self.switchView.isOn = switchIsOn
            self.switchView.isHidden = false
        } else {
            self.switchView.isOn = false
            self.switchView.isHidden = true
        }
        self.switchView.isEnabled = !model.switchIsReadOnly
        self.label.text = model.labelText
    }
    
    @IBAction func switchAction(_ sender: Any) {
        guard let model = self.model as? BasicCells.SwitchCellModel else {
            return
        }
        let sender = (sender as! UISwitch)
        model.switchIsOn = sender.isOn
        model.switchCommand?.perform()
    }
}
