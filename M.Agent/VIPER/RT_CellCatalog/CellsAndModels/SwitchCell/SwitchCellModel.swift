//
//  SwitchCellModel.swift
//  RT_CellCatalogPod
//
//  Created by Вова Петров on 26/12/2018.
//  Copyright © 2018 DartIT. All rights reserved.
//

import UIKit

public enum SwitchCellModelType: CellModelTypeProtocol {
    case horizontal
    public var nibNameAndReuseIdentifier: (String, String) {
        switch self {
        case .horizontal:
            return ("SwitchCell", "switchCell")
        }
    }
}

public extension BasicCells {
    public final class SwitchCellModel: BaseCellModel {

        public var switchIsOn: Bool?
        public var switchIsReadOnly: Bool
        public var labelText: String
        public var switchCommand: TableSectionViewModelCommand?
        public private(set) var type: SwitchCellModelType
        
        public init(labelText: String, switchIsOn: Bool?, type: SwitchCellModelType = .horizontal, style: CellModelStyle? = nil) {
            self.switchIsOn = switchIsOn
            self.switchIsReadOnly = false
            self.labelText = labelText
            self.type = type
            
            super.init(reuseIdentifier: type.nibNameAndReuseIdentifier.1)
            initStyle(style)
            initTypes(with: SwitchCellModelType.allCases)
        }
    }
}
