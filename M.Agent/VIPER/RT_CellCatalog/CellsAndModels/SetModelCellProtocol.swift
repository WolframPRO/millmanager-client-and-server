//
//  SetModelCellProtocol.swift
//  RT_CellCatalogPod
//
//  Created by Вова Петров on 26/12/2018.
//  Copyright © 2018 DartIT. All rights reserved.
//

public protocol SetModelCellProtocol: class {
    func set(model: BaseCellModel)
    func update()
}
