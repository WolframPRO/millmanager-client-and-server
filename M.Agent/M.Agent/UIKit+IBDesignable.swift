//
//  UIKit+IBDesignable.swift
//  MobileAgent
//
//  Created by Вова Петров on 31/01/2019.
//  Copyright © 2019 DartIT. All rights reserved.
//

import UIKit

extension UIImageView {
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        tintColorDidChange()
    }
    
    @IBInspectable var imageColor: UIColor! {
        set {
            super.tintColor = newValue
        }
        get {
            return super.tintColor
        }
    }
} 

@IBDesignable class CustomButton : UIButton {
}

@IBDesignable class CustomView : UIView {
}

@IBDesignable class CustomLabel : UILabel {
}

@IBDesignable class CustomTextField: UITextField {
    
    @IBInspectable var isPasteEnabled: Bool = true
    
    @IBInspectable var isSelectEnabled: Bool = true
    
    @IBInspectable var isSelectAllEnabled: Bool = true
    
    @IBInspectable var isCopyEnabled: Bool = true
    
    @IBInspectable var isCutEnabled: Bool = true
    
    @IBInspectable var isDeleteEnabled: Bool = true
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        switch action {
        case #selector(UIResponderStandardEditActions.paste(_:)) where !isPasteEnabled,
             #selector(UIResponderStandardEditActions.select(_:)) where !isSelectEnabled,
             #selector(UIResponderStandardEditActions.selectAll(_:)) where !isSelectAllEnabled,
             #selector(UIResponderStandardEditActions.copy(_:)) where !isCopyEnabled,
             #selector(UIResponderStandardEditActions.cut(_:)) where !isCutEnabled,
             #selector(UIResponderStandardEditActions.delete(_:)) where !isDeleteEnabled:
            return false
        default:
            //return true : this is not correct
            return super.canPerformAction(action, withSender: sender)
        }
    }
}

@IBDesignable class CustomTextView : UITextView {
}

@IBDesignable class CustomImageView : UIImageView {
}
