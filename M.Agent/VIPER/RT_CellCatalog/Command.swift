//
//  Command.swift
//  M.Agent
//
//  Created by Владимир on 18/03/2019.
//  Copyright © 2019 Wolfram. All rights reserved.
//

class Command {
    var command: () -> ()
    
    init(command: @escaping () -> ()) {
        self.command = command
    }
    
    func perform() {
        command()
    }
    
    func performOnMain() {
        DispatchQueue.main.async { [weak self] in
            self?.command()
        }
    }
}
