//
//  AccountShowModel.swift
//  M.Agent
//
//  Created by Владимир on 19/03/2019.
//  Copyright © 2019 Wolfram. All rights reserved.
//

import Foundation
import EVReflection
import Alamofire

struct AccountShowModel: Codable {
    var id: String
    var allowdeleteitem: String = "off"
    var list: String?
    var ignoreitemmax: String = "off"
    var attitude: String?
    var new_abuse_rate: String?
    var name: String
    var old_abuse_rate: String?
    var saved_filters: String?
    var elid: String
    var allowsuspenditem: String?
    var registration_ip: String?
    var note: String?
    var registration_date: String //Дата регистрации
    var valid_phone: String?
    var ignoreabuserate: String?
    var balance: String
    var abuse_rate: String?
    var nocalcstat: String?
    var country: String?
}
