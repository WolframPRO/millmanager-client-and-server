//
//  ContractListRouter.swift
//  M.Agent
//
//  Created by Владимир on 21/03/2019.
//Copyright © 2019 Wolfram. All rights reserved.
//

import Foundation


final class ContractListRouter: RouterExtended {
    func showDetailAccount(id: String) {
        setupAndShow(module: AppModules.AccountEdit, with: AccountEditPresenterSetup.init(elid: id, closeFeedback: FeedbackHolder(feedbackID: "", feedbackBlock: {[weak self] (presenter, str) in
            self?.presenter.reloadData()
        })), for: AccountEditPresenter.self)
    }
    func showAddAccount(setupData: AccountAddPresenterSetup) {
        setupAndShow(module: AppModules.AccountAdd, with: setupData, for: AccountAddPresenter.self)
    }
}

extension ContractListRouter: FeedbackProtocol {
    var feedbackID: String {
        return "expectedFeedbackInRouter"
    }
    func receiveFeedbackFrom(_ presenter: PresenterAPIProtocol, forFeedbackID: String) {
        /*
         if let _ = presenter as? SecondPresenterAPI {
         */
        self.presenter.refreshTableState()
        /*
         }
         */
        
    }
}


// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension ContractListRouter {
    var presenter: ContractListPresenter {
        return _presenter as! ContractListPresenter
    }
}


