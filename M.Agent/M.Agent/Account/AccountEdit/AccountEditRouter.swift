//
//  AccountEditRouter.swift
//  M.Agent
//
//  Created by Владимир on 18/03/2019.
//Copyright © 2019 Wolfram. All rights reserved.
//

import Foundation


final class AccountEditRouter: RouterExtended {
    /*
     func showSecondModule(with setupData: SecondPresenterSetup) {
     
     setupAndShow(module: AppModules.second,
     with: setupData,
     for: SecondPresenter.self)
     
     }
     */
}

extension AccountEditRouter: FeedbackProtocol {
    var feedbackID: String {
        return "expectedFeedbackInRouter"
    }
    func receiveFeedbackFrom(_ presenter: PresenterAPIProtocol, forFeedbackID: String) {
        /*
         if let _ = presenter as? SecondPresenterAPI {
         */
        self.presenter.refreshTableState()
        /*
         }
         */
        
    }
}


// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension AccountEditRouter {
    var presenter: AccountEditPresenter {
        return _presenter as! AccountEditPresenter
    }
}


