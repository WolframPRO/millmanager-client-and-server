//
//  AccountAddInteractor.swift
//  M.Agent
//
//  Created by Владимир on 18/03/2019.
//Copyright © 2019 Wolfram. All rights reserved.
//

import Foundation
import Alamofire
import CodableFirebase

final class AccountAddInteractor: BaseTableInteractor {
    var someImportantParam: String? = nil
    
    override func load() {
        super.load()
    }
    
    func createClient(completion: @escaping (Bool) -> Void) {
        do {
            let account = try FirebaseEncoder().encode(presenter.account) as! Parameters
            manager.request(bill("func=account.edit&sok=ok"),
                            method: .get,
                            parameters: account).responseJSON { (response) in
                switch response.result {
                case .success:
                    if let value = (response.result.value as? [String: Any]) {
                        if let error = value["error"] as? [String: Any]{
                                if let _ = error["msg"] as? String {
                                completion(false)
                                return
                            }
                        }
                    }
                    completion(true)
                case .failure:
                    completion(false)
                }
                
            }
        } catch {}
    }
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension AccountAddInteractor {
    var presenter: AccountAddPresenter {
        return _presenter as! AccountAddPresenter
    }
}
