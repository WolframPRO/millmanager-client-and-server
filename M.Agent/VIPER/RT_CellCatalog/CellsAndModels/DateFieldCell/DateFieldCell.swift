//
//  DateFieldCell.swift
//  RT_CellCatalogPod
//
//  Created by Вова Петров on 07/02/2019.
//Copyright © 2019 Rostelecom. All rights reserved.
//

import UIKit

final class DateFieldCell: SetModelBaseCell {

    //MARK: - Outlets
    //@IBOutlet weak var oneLabel: UILabel!
    @IBOutlet weak var textFieldLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: CustomTextField!
    private let dateFormatter = DateFormatter()
    var toolBar: UIToolbar?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func update() {
        super.update()
        guard let model = self.model as? BasicCells.DateFieldCellModel else {
            return
        }
        dateFormatter.dateFormat = model.dateFormat
        label.text = model.labelText
        textField.text = dateFormatter.string(from: model.fieldDate ?? Date())
        
        label(hide: model.labelText.count == 0)
    }
    
    fileprivate func label(hide: Bool){
        if let leftConstraint = self.textFieldLeftConstraint {
            leftConstraint.isActive = !hide
            self.contentView.layoutIfNeeded()
        }
    }
    
    @objc private func datePickerValueChanged(sender:UIDatePicker) {
        textField.text = dateFormatter.string(from: sender.date)
        guard let model = self.model as? BasicCells.DateFieldCellModel else {
            return
        }
        model.selectedBlock(sender.date)

    }
    @IBAction func beginEditing(_ sender: Any) {
        guard let model = self.model as? BasicCells.DateFieldCellModel else {
            return
        }
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = model.datePickerMode
        datePicker.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
        if model.maxDate != nil {
            datePicker.maximumDate = model.maxDate
        }
        if model.minDate != nil {
            datePicker.minimumDate = model.minDate
        }
        if model.showToolbar {
            createToolbar(datePicker: datePicker)
        }
        textField.inputView = datePicker
        model.beginEditingBlock(datePicker.bounds.height)
    }
    
    @IBAction func endEgiting(_ sender: Any) {
        guard let model = self.model as? BasicCells.DateFieldCellModel else {
            return
        }
        model.endEditingBlock()
    }
    override func haveCommandAccessoryType() -> UITableViewCell.AccessoryType {
        return .disclosureIndicator
    }

    
    func createToolbar(datePicker: UIDatePicker){
        guard let model = self.model as? BasicCells.DateFieldCellModel else {
            return
        }
        
        self.toolBar = UIToolbar(frame: CGRect(x: 0, y:  datePicker.frame.size.height/6, width: datePicker.frame.size.width, height: 40))
        
        self.toolBar!.layer.position = CGPoint(x: datePicker.frame.size.width/2, y: datePicker.frame.size.height-20.0)
        self.toolBar!.barStyle = .default
    
        let todayBtn = UIBarButtonItem(title: "Сегодня", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.tappedToolBarBtn))
        let okBarBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(self.donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil)
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: datePicker.frame.size.width / 3, height: 14))
        label.text = model.labelText
        label.textAlignment = NSTextAlignment.center
        
        let textBtn = UIBarButtonItem(customView: label)
        if model.showTodayButton {
            self.toolBar!.setItems([todayBtn,flexSpace,textBtn,flexSpace,okBarBtn], animated: true)
        } else {
            self.toolBar!.setItems([flexSpace,flexSpace,textBtn,flexSpace,okBarBtn], animated: true)
        }
        
        textField.inputAccessoryView = self.toolBar!
    }
    
    @objc func donePressed(sender: UIBarButtonItem) {
        textField.resignFirstResponder()
    }
    
    @objc func tappedToolBarBtn(sender: UIBarButtonItem) {
        textField.text = dateFormatter.string(from: Date())
        textField.resignFirstResponder()
    }
    

}
