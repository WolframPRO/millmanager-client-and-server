//
//  ButtonCellModel.swift
//  RT_CellCatalogPod
//
//  Created by Вова Петров on 18/01/2019.
//  Copyright © 2019 DartIT. All rights reserved.
//

import UIKit

public enum ButtonCellModelType: CellModelTypeProtocol {
    case regular
    public var nibNameAndReuseIdentifier: (String, String) {
        switch self {
        case .regular:
            return ("ButtonCell", "buttonCell")
        }
    }
}

public extension BasicCells {
    public final class ButtonCellModel: BaseCellModel {

        public var labelText: String
        public var activityIndicatorAnimating: Bool
        public private(set) var type: ButtonCellModelType
        
        public init(labelText: String, type: ButtonCellModelType = .regular, style: CellModelStyle? = nil) {
            self.labelText = labelText
            self.activityIndicatorAnimating = false
            self.type = type
            
            super.init(reuseIdentifier: type.nibNameAndReuseIdentifier.1)
            initStyle(style)
            initTypes(with: ButtonCellModelType.allCases)
        }
    }
}

