//
//  SwitchCellModelAgentStyle.swift
//  RT_CellCatalogPod
//
//  Created by Alexey Bykov on 2/1/19.
//  Copyright © 2019 Rostelecom. All rights reserved.
//

import Foundation
import UIKit

public final class SwitchCellModelAgentStyle: CellModelStyleBase {
    required public init(with styleToBeDecorated: CellModelStyleProtocol?) {
        super.init(with: styleToBeDecorated)
    }
    override public func imbue() {
        super.imbue()
        // my imbue
        if  let model = self.model,
            let cell = self.model?.cell as? SwitchCell {
            cell.label.textColor = model.isReadOnly ? UIColor.agent.text.readOnly : UIColor.agent.text.common
        }
    }
}
