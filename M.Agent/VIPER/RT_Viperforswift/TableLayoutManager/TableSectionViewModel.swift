//
//  TableSectionViewModel.swift
//  Example
//
//  Created by Alexey Bykov on 11/27/18.
//

import Foundation
import UIKit

open class TableSectionViewModel {
    private var rowFilterBlock: ((_ viewModel: TableSectionViewModel) -> Bool)?
    internal private(set) var id: String?
    weak internal private(set) var tableLayoutManager: TableLayoutManager?
    
    weak public private(set) var tableView: UITableView?
    public private(set) var htmlLinkBlock: ((_ url: URL, _ text: String) -> Void)?
    
    //MARK: - Initialization
    
    func setup(withTableView: UITableView, tableLayoutManager: TableLayoutManager) {
        self.tableView = withTableView
        self.tableLayoutManager = tableLayoutManager
    }
    
    func setup(withId: String) {
        self.id = withId
    }
    
    public init(withRowFilterBlock: ((_ viewModel: TableSectionViewModel) -> Bool)? = nil) {
        self.tableView = nil
        self.rowFilterBlock = withRowFilterBlock
    }
    
    //MARK: - Utilities
    
    func rowFilterBlockCalledAndFilteredOut() -> Bool {
        if let block = rowFilterBlock {
            if !block(self) {
                return true
            }
        }
        return false
    }
    
    /// Reloads rows, optionally animated
    private func doReload(rows:[Int]?, animated: Bool) {
        if let section = tableLayoutManager?.sectionForSectionViewModel(self) {
            if let rowIndexes = rows,
                let table = tableView,
                let numOfRows = tableLayoutManager?.tableView(table, numberOfRowsInSection: section)
            {
                let indexPaths = rowIndexes.filter({ (rowIndex) -> Bool in
                    return rowIndex >= 0 && rowIndex < numOfRows
                }).map {
                    IndexPath(row: $0, section: section)
                }
                if indexPaths.count > 0 {
                    tableView?.reloadRows(at: indexPaths, with: animated ? UITableView.RowAnimation.automatic : UITableView.RowAnimation.none)
                }
            } else {
                tableView?.reloadSections(IndexSet.init(integer: section), with: animated ? UITableView.RowAnimation.automatic : UITableView.RowAnimation.none)
            }
        }
    }
    
    /// Reloads all rows and headers/footers, optionally animated
    public func reload(animated: Bool = true) {
        doReload(rows: nil, animated: animated)
    }
    
    /// Reloads rows, optionally animated
    public func reload(rows:[Int], animated: Bool = true) {
        doReload(rows: rows, animated: animated)
    }
    
    public func hideAtTheBottom(numberOfRows: Int, animated: Bool = true) {
        tableLayoutManager?.hideAtTheBottom(for: self, numberOfRows: numberOfRows, animated: animated)
    }
    
    public func showAllRows(animated: Bool = true) {
        tableLayoutManager?.showAllRows(for: self, animated: animated)
    }
    
    //MARK: - Table view data source & delegate helpers
    
    /// must be overriden
    open func numberOfRows() -> Int {
        print(TableLayoutError.methodNotImplemented.description)
        abort()
    }
    
    /// can be overriden
    open func heightForHeader() -> CGFloat {
        if rowFilterBlockCalledAndFilteredOut() {
            return 0.1
        }
        return -1
    }
    
    /// can be overriden
    open func heightForFooter() -> CGFloat {
        if rowFilterBlockCalledAndFilteredOut() {
            return 0.1
        }
        return -1
    }
    
    /// can be overriden
    open func titleForHeader() -> String? {
        if rowFilterBlockCalledAndFilteredOut() {
            return ""
        }
        return nil
    }
    
    /// can be overriden
    open func titleForFooter() -> String? {
        if rowFilterBlockCalledAndFilteredOut() {
            return ""
        }
        return nil
    }
    
    /// can be overriden
    open func heightForRow(row: Int) -> CGFloat {
        if rowFilterBlockCalledAndFilteredOut() {
            return 0
        }
        return -1
    }
    
    /// must be overriden
    open func cellForRowAtIndexPath(indexPath: IndexPath) -> UITableViewCell {
        print(TableLayoutError.methodNotImplemented.description)
        abort()
    }
    
    /// may be overriden optionally
    open func didSelectRowAtIndexPath(indexPath: IndexPath) {
        
    }
    
    /// may be overriden optionally
    open func accessoryButtonTappedForRowWith(indexPath: IndexPath) {
        
    }
    
    /// may be overriden optionally
    open func viewForHeader() -> UIView? {
        return nil
    }
    
    /// may be overriden optionally
    open func viewForFooter() -> UIView? {
        return nil
    }
    
    
    /// Rearranging support: may be overriden optionally
    open func canEditRowAtIndexPath(indexPath: IndexPath) -> Bool {
        return false
    }
    /// Rearranging support: may be overriden optionally
    open func commit(editingStyle: UITableViewCell.EditingStyle, forRowAtIndexPath: IndexPath) {
    }
    /// Rearranging support: may be overriden optionally
    open func moveRowAtIndexPath(fromIndexPath: IndexPath, toIndexPath: IndexPath) {
        
    }
    /// Rearranging support: may be overriden optionally
    open func canMoveRowAtIndexPath(indexPath: IndexPath) -> Bool {
        return false
    }
    
    
    //MARK: - Support for attributed headers and footers
    
    /// may be overriden optionally
    open func attributedTitleForHeader() -> NSAttributedString? {
        return nil
    }
    
    /// may be overriden optionally
    open func attributedTitleForFooter() -> NSAttributedString? {
        return nil
    }
    
    
    //MARK: - Support for html headers and footers
    
    /// may be overriden optionally
    open func htmlTitleForHeader() -> String? {
        return nil
    }
    
    /// may be overriden optionally
    open func htmlTitleForFooter() -> String? {
        return nil
    }
    
    /// may be overriden optionally
    open func htmlTitleHeaderFont() -> UIFont? {
        return nil
    }
    
    /// may be overriden optionally
    open func htmlTitleFooterFont() -> UIFont? {
        return nil
    }
    
    /// may be overriden optionally
    open func htmlLinkColorName() -> String? {
        return nil
    }
    
    /// may be assigned to optionally
    open func setHtmlLinkBlock(_ block: @escaping (_ url: URL, _ text: String) -> Void) {
        htmlLinkBlock = block
    }
    
}

