//
//  UIC.swift
//  RT_CellCatalogPod
//
//  Created by Вова Петров on 15/02/2019.
//  Copyright © 2019 Rostelecom. All rights reserved.
//

import UIKit

///Example: UIColor.agent.text.common
extension UIColor {
    class var rtRed: UIColor {
        return UIColor(red: 229.0/255.0, green: 0.0, blue: 83.0/255.0, alpha: 1)
    }
    class var rtBlue: UIColor {
        return UIColor(red: 0.0/255.0, green: 122.0, blue: 255.0/255.0, alpha: 1)
    }
    class var agentBlue: UIColor {
        return UIColor(red: 0.0/255, green: 80.0/255, blue: 255.0/255, alpha: 1)
    }
    
    class agent {
        class text {
            class var important: UIColor {
                return agentBlue
            }
            class var common: UIColor {
                return black
            }
            class var readOnly: UIColor {
                return gray
            }
            class var placeholder: UIColor {
                return lightGray
            }
        }
        
        class button {
            class var active: UIColor {
                return agentBlue
            }
            class var disabled: UIColor {
                return gray
            }
            class var cancel: UIColor {
                return rtRed
            }
        }
        
        class link {
            class var active: UIColor {
                return agentBlue
            }
            class var used: UIColor {
                return purple
            }
            class var disabled: UIColor {
                return gray
            }
        }
        
    }
}
