//
//  ContractListView.swift
//  M.Agent
//
//  Created by Владимир on 21/03/2019.
//Copyright © 2019 Wolfram. All rights reserved.
//

import UIKit

//MARK: - Public Interface Protocol
protocol ContractListViewInterface {
    var myDisplayData: ContractListDisplayData { get }
    func deselectSelectedRow()
    func errorCreate()
}

//MARK: ContractListView Class
final class ContractListView: BaseTableView {
    @IBOutlet weak var mainLabel: UILabel?
    override func update() {
        super.update()
        mainLabel?.text = displayData.mainTitle
    }
    @IBAction func addContractAction(_ sender: Any) {
        self.myDisplayData.addCommand?.perform()
    }
}

//MARK: - Public interface
extension ContractListView: ContractListViewInterface {
    var myDisplayData: ContractListDisplayData {
        return displayData
    }
    
    func errorCreate() {
        DispatchQueue.main.async {
            let cancelAction = UIAlertAction(title: "Ок", style: .default) { (_) -> Void in
            }
            let alert = UIAlertController(title: "Ошибка", message: "При создании договора возникла ошибка", preferredStyle: .alert)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension ContractListView {
    var displayData: ContractListDisplayData {
        return _displayData as! ContractListDisplayData
    }
}



