//
//  TwoLabelCellTableViewCell.swift
//  RT_CellCatalogPod
//
//  Created by Вова Петров on 26/12/2018.
//  Copyright © 2018 DartIT. All rights reserved.
//

import UIKit

final class TwoLabelCell: SetModelBaseCell {

    @IBOutlet weak var oneLabel: UILabel!
    @IBOutlet weak var twoLabel: UILabel!
    @IBOutlet weak var oneLabelRightDisabledConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func update() {
        super.update()
        guard let model = self.model as? BasicCells.TwoLabelCellModel else {
            return
        }
        
        oneLabel.text = model.oneLabelText
        twoLabel.text = model.twoLabelText
        twoLabel(hide: model.twoLabelText.count == 0)
    }
    
    fileprivate func twoLabel(hide: Bool){
        if let rightConstraint = self.oneLabelRightDisabledConstraint {
            rightConstraint.isActive = !hide
            self.contentView.layoutIfNeeded()
        }
    }
    
    override func haveCommandAccessoryType() -> UITableViewCell.AccessoryType {
        return .disclosureIndicator
    }

}
