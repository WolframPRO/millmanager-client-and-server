//
//  SetModelBaseCell.swift
//  RT_CellCatalogPod
//
//  Created by Вова Петров on 19/01/2019.
//  Copyright © 2019 DartIT. All rights reserved.
//

import UIKit

open class SetModelBaseCell: UITableViewCell, SetModelCellProtocol {

    public private(set) var model: BaseCellModel!
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override open func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    open func update() {
        guard let model = self.model else {
            return
        }
        
        self.isSelected = model.isSelected
        self.isUserInteractionEnabled = !model.isReadOnly
        
        //Определяем accessoryType по логическому состоянию ячейки
        if model.isReadOnly {
            self.accessoryType = .none
            self.selectionStyle = .none
        } else {
            if model.isCheck {
                self.accessoryType = .checkmark
                self.selectionStyle = .none
            } else {
                if model.selectCommand != nil {
                    self.accessoryType = haveCommandAccessoryType()
                    self.selectionStyle = .default
                } else {
                    self.accessoryType = .none
                    self.selectionStyle = .none
                }
            }
        }
        self.model.style?.imbue()
    }
    
    open func haveCommandAccessoryType() -> UITableViewCell.AccessoryType {
        return .none
    }
    
    public func set(model: BaseCellModel) {
        self.model = model
        self.model.cell = self
        self.update()
    }
}
