//
//  ButtonCellModelAgentStyle.swift
//  RT_CellCatalogPod
//
//  Created by Alexey Bykov on 1/30/19.
//  Copyright © 2019 DartIT. All rights reserved.
//

import Foundation
import UIKit

public final class ButtonCellModelAgentStyle: CellModelStyleBase {
    required public init(with styleToBeDecorated: CellModelStyleProtocol?) {
        super.init(with: styleToBeDecorated)
    }
    override public func imbue() {
        super.imbue()
        // my imbue
        if let model = self.model,
           let cell = self.model?.cell as? ButtonCell {
            cell.label.textColor =  model.isReadOnly ? UIColor.agent.button.disabled : UIColor.agent.button.active
        }
    }
    
}
