//
//  TableLayoutProtocols.swift
//  Example
//
//  Created by Alexey Bykov on 12/4/18.
//

import Foundation

public protocol TableDisplayData {
    associatedtype TableSectionViewModelType
    func setupSections(_ sections:[TableSectionViewModelType]?)
}

public protocol TablePresenterBuilder {
    associatedtype TableSectionViewModelType
    func buildSections() -> [TableSectionViewModelType]
}

