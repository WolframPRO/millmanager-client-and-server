//
//  UITableView+TextHeight.h
//  OnLime
//
//  Created by bykov-av on 06/02/2017.
//  Copyright © 2017 Dart-IT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (RowHeight)

// specific:

- (CGFloat)heightOfNormalText:(NSString*)text;
- (CGFloat)heightOfNormalText:(NSString*)text withMargin:(CGFloat)margin;
- (CGFloat)widthOfNormalText:(NSString *)text;

- (CGFloat)heightOfPricelText:(NSString*)text;
- (CGFloat)heightOfPriceText:(NSString*)text withMargin:(CGFloat)margin;
- (CGFloat)widthOfPricelText:(NSString *)text;

- (CGFloat)heightOfMenuText:(NSString*)text;
- (CGFloat)heightOfMenuText:(NSString*)text withMargin:(CGFloat)margin;
- (CGFloat)heightOfMenuText:(NSString*)text withMargin:(CGFloat)margin truncatedByNumberOfLines:(NSUInteger)numberOfLines;
- (CGFloat)widthOfMenuText:(NSString *)text;

- (CGFloat)heightOfSemiboldMenuText:(NSString*)text;
- (CGFloat)heightOfSemiboldMenuText:(NSString*)text withMargin:(CGFloat)margin;
- (CGFloat)heightOfSemiboldMenuText:(NSString*)text withMargin:(CGFloat)margin truncatedByNumberOfLines:(NSUInteger)numberOfLines;

- (CGFloat)heightOfSmallText:(NSString*)text;
- (CGFloat)heightOfSmallText:(NSString*)text withMargin:(CGFloat)margin;
- (CGFloat)heightOfSmallText:(NSString*)text withMargin:(CGFloat)margin truncatedByNumberOfLines:(NSUInteger)numberOfLines;
- (CGFloat)widthOfSmallText:(NSString *)text;


// universal:

- (CGFloat)heightOfText:(NSString*)text withMargin:(CGFloat)margin ofFontSize:(CGFloat)fontSize;
- (CGFloat)heightOfText:(NSString*)text withMargin:(CGFloat)margin ofFontSize:(CGFloat)fontSize truncatedByNumberOfLines:(NSUInteger)numberOfLines;
- (CGFloat)heightOfText:(NSString*)text withMargin:(CGFloat)margin ofFontSize:(CGFloat)fontSize semiBold:(BOOL)semiBold;
- (CGFloat)heightOfText:(NSString*)text withMargin:(CGFloat)margin ofFontSize:(CGFloat)fontSize semiBold:(BOOL)semiBold truncatedByNumberOfLines:(NSUInteger)numberOfLines;
- (CGFloat)widthOfText:(NSString*)text withMargin:(CGFloat)margin ofFontSize:(CGFloat)fontSize semiBold:(BOOL)semiBold;

@end
