//
//  TableLayoutError.swift
//  Example
//
//  Created by Alexey Bykov on 11/27/18.
//

import Foundation

enum TableLayoutError : Error {
    case methodNotImplemented
    
    var description: String {
        var message = ""
        switch self {
        case .methodNotImplemented: message = "Method not implemented"
        }
        return "[TableLayout WARNING]: \(message)"
    }
}
