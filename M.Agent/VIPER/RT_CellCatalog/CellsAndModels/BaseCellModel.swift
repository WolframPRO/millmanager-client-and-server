//
//  BaseCellModel.swift
//  RT_CellCatalogPod
//
//  Created by Вова Петров on 26/12/2018.
//  Copyright © 2018 DartIT. All rights reserved.
//

import UIKit

open class BaseCellModel: NSObject {
    public var height: CGFloat
    public var selectCommand: TableSectionViewModelCommand?
    public var isSelected: Bool
    public var isReadOnly: Bool
    public var isCheck: Bool
    
    weak public internal(set) var cell: SetModelCellProtocol?

    public func updateView(){
        if let cell = cell {
            cell.update()
        }
    }

    public init(reuseIdentifier: String) {
        self.height = UITableView.automaticDimension
        self.reuseIdentifier = reuseIdentifier
        self.isSelected = false
        self.isReadOnly = false
        self.isCheck = false
    }
    
    public func initStyle(_ style: CellModelStyle?) {
        let currentStyle = style ?? CellModelStyle.defaultStyle
        self.style = currentStyle.build(for: self, applying: [type(of: self), BaseCellModel.self])
    }
    
    public func initTypes<T: CellModelTypeNamesProtocol>(with types: [T]) {
        CellModelTypes.register(types: types)
    }
    
    // internals
    internal private(set) var reuseIdentifier: String
    internal private(set) var style: CellModelStyleProtocol? = nil
}

extension BaseCellModel: CellModelStyleProtocol {
    public var model: BaseCellModel? {
        return self
    }
    public func imbue() {
        guard let cell = self.cell as? SetModelBaseCell else {
            return
        }
        cell.contentView.backgroundColor = UIColor.white
    }
}

public enum BasicCells {
    
}
