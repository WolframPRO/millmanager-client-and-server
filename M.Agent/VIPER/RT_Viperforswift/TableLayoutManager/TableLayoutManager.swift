//
//  TableLayoutManager.swift
//  Example
//
//  Created by Alexey Bykov on 11/27/18.
//

import Foundation
import UIKit

open class TableLayoutManager: NSObject {
    
    weak private var tableView: UITableView?
    
    fileprivate struct Segment {
        let name: String
        var sections: [TableSectionViewModel]
    }
    fileprivate var segments: [Segment]? = nil
    
    fileprivate var labelDictionary: [Int : UIView]? = nil
    fileprivate var labelFooterDictionary: [Int : UIView]? = nil
    
    fileprivate var rtlabelDictionary: [Int : UIView]? = nil
    fileprivate var rtlabelFooterDictionary: [Int : UIView]? = nil
    
    fileprivate var numberOfRowsForSectionId: [String:Int?] = [:]
    
    // rounded mode vars
    fileprivate var separatorColor: UIColor? = nil
    fileprivate var separatorStyle: UITableViewCell.SeparatorStyle? = nil
    fileprivate var origLeftMargin: CGFloat? = nil
    fileprivate var origRightMargin: CGFloat? = nil
    
    fileprivate var useRoundedMode: Bool = false
    fileprivate var useRoundedModeActive: Bool = false
    fileprivate var leftMargin: CGFloat = 15
    fileprivate var rightMargin: CGFloat = 15
    
    func empty(tableView: UITableView) {
        self.tableView = tableView
        tableView.dataSource = self
        tableView.delegate = self
        segments = []
    }
    
    func eatSectionModels(_ sections:[TableSectionViewModel], forTableView tableView: UITableView) {
        self.tableView = tableView
        tableView.dataSource = self
        tableView.delegate = self
        for section in sections {
            section.setup(withTableView: tableView, tableLayoutManager: self)
        }
        
        // continue eating...
        
        guard let firstModel = sections.first else { return }
        let className = String(describing:type(of:firstModel))
        guard !className.isEmpty else { return }
        
        var foundSegmentIndex: Int? = nil
        var segmentIndex: Int = 0
        segments = segments?.map {
            if $0.name == className {
                foundSegmentIndex = segmentIndex
                segmentIndex = segmentIndex + 1
                return Segment(name: className, sections: sections)
            } else {
                segmentIndex = segmentIndex + 1
                return $0
            }
        }
        
        var sectionIndex = 0
        for section in sections {
            section.setup(withId: "\(foundSegmentIndex ?? segmentIndex)\(sectionIndex)")
            sectionIndex = sectionIndex + 1
        }
        
        guard foundSegmentIndex == nil else { return }
        
        var tempSegments = segments ?? []
        tempSegments.append(Segment(name: className, sections: sections))
        segments = tempSegments
    }
    
    
    func useRoundedMode(useIt: Bool, leftMargin: CGFloat, rightMargin: CGFloat, forTableView tableView: UITableView) {
        self.tableView = tableView
        self.useRoundedMode = useIt
        self.leftMargin = leftMargin
        self.rightMargin = rightMargin
        if useRoundedMode {
            NotificationCenter.default.addObserver(self, selector: #selector(orientationDidChange), name: UIDevice.orientationDidChangeNotification, object: nil)
        } else {
            NotificationCenter.default.removeObserver(self)
        }
        doRoundedMode(useRoundedModeActive: useRoundedMode && roundedModeCanBeActive())
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func sectionViewModelForSection(_ section: Int) -> TableSectionViewModel? {
        var count: Int = 0
        for segment in segments ?? [] {
            if section < count + segment.sections.count {
                return segment.sections[section - count];
            }
            count += segment.sections.count
        }
        return nil
    }
    
    func normalizedSectionForSection(_ section: Int) -> Int {
        var count: Int = 0
        for segment in segments ?? [] {
            if section < count + segment.sections.count {
                return section - count;
            }
            count += segment.sections.count
        }
        return section
    }
    
    func sectionForSectionViewModel(_ sectionViewModel: TableSectionViewModel) -> Int? {
        var sectionIndex: Int = 0
        for segment in segments ?? [] {
            for section in segment.sections {
                if section.id == sectionViewModel.id {
                    return sectionIndex
                }
                sectionIndex = sectionIndex + 1
            }
        }
        return nil
    }
    
    func hideAtTheBottom(for sectionViewModel: TableSectionViewModel, numberOfRows: Int, animated: Bool) {
        let currentNumberOfRows = sectionViewModel.numberOfRows()
        
        guard let id = sectionViewModel.id,
            numberOfRowsForSectionId[id] == nil,
            numberOfRows >= 0 && numberOfRows <= currentNumberOfRows,
            let section = sectionForSectionViewModel(sectionViewModel)
            else { return }
        
        let numberOfRowsLeft = currentNumberOfRows - numberOfRows
        numberOfRowsForSectionId[id] = numberOfRowsLeft
        
        var indexPaths:[IndexPath] = []
        for row in numberOfRowsLeft..<currentNumberOfRows  {
            indexPaths.append(IndexPath(row: row, section: section))
        }
        tableView?.deleteRows(at: indexPaths, with: animated ? UITableView.RowAnimation.automatic : UITableView.RowAnimation.none)
    }
    
    func showAllRows(for sectionViewModel: TableSectionViewModel, animated: Bool) {
        let numberOfRowsRestored = sectionViewModel.numberOfRows()
        
        guard let id = sectionViewModel.id,
            let section = sectionForSectionViewModel(sectionViewModel),
            let numberOfRowsForSection = numberOfRowsForSectionId[id],
            let currentNumberOfRows = numberOfRowsForSection,
            currentNumberOfRows >= 0 && currentNumberOfRows <= numberOfRowsRestored
            else { return }
        
        let indexOfRowToStartInsert = numberOfRowsRestored - currentNumberOfRows
        numberOfRowsForSectionId[id] = nil
        
        var indexPaths:[IndexPath] = []
        for row in indexOfRowToStartInsert..<numberOfRowsRestored  {
            indexPaths.append(IndexPath(row: row, section: section))
        }
        tableView?.insertRows(at: indexPaths, with: animated ? UITableView.RowAnimation.automatic : UITableView.RowAnimation.none)
    }
    
}


//MARK: - UITableView data source and delegate
extension TableLayoutManager: UITableViewDataSource,  UITableViewDelegate {
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        var count: Int = 0
        for segment in segments ?? [] {
            count += segment.sections.count
        }
        if useRoundedMode {
            doRoundedMode(useRoundedModeActive: roundedModeCanBeActive())
        }
        return count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionModel = self.sectionViewModelForSection(section)
        
        if let id = sectionModel?.id,
            let numberOfRowsForSection = numberOfRowsForSectionId[id],
            let numberOfRows = numberOfRowsForSection
        {
            return numberOfRows
        }
        
        return sectionModel?.numberOfRows() ?? 0
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let sectionModel = self.sectionViewModelForSection(section)
        
        var deltaH: CGFloat = 0
        if let _ = tableView.tableHeaderView,
            section == 0
        {
            if useRoundedModeActive && roundedModeCanBeActive() {
                deltaH = 10
            } else {
                deltaH = 0
            }
        }
        
        
        let h = sectionModel?.heightForHeaderDefault() ?? 0
        if h >= 0 {
            return h + deltaH
        }
        let height = sectionModel?.heightForHeader() ?? 0
        
        if let title = sectionModel?.attributedTitleForHeader(),
            (height < 0 || height > 1)
        {
            let rect = title.boundingRect(with: CGSize(width: tableView.bounds.size.width -
                (self.isLandscape() ? 40 : 30),
                                                       height: CGFloat.greatestFiniteMagnitude),
                                          options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                          context: nil)
            return rect.size.height + 25 + deltaH
            
        } else if let title = sectionModel?.htmlTitleForHeader(),
            (height < 0 || height > 1)
        {
            let font = sectionModel?.htmlTitleHeaderFont() ?? UIFont.systemFont(ofSize: 14.0)
            
            let titleStripped = title.stripHTML()
            
            let rect = titleStripped.boundingRect(with: CGSize(width: tableView.bounds.size.width - 30,
                                                               height: CGFloat.greatestFiniteMagnitude),
                                                  options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                  attributes: [NSAttributedString.Key.font : font],
                                                  context: nil)
            return rect.size.height + 50 + deltaH
        }
        if height >= 0 {
            return height + deltaH
        }
        return -1 + deltaH
    }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let sectionModel = self.sectionViewModelForSection(section)
        
        let h = sectionModel?.heightForFooterDefault() ?? 0
        if h >= 0 {
            return h
        }
        let height = sectionModel?.heightForFooter() ?? 0
        
        if let title = sectionModel?.attributedTitleForFooter(),
            (height < 0 || height > 1)
        {
            let rect = title.boundingRect(with: CGSize(width: tableView.bounds.size.width -
                (self.isLandscape() || self.iPhone6PlusAndGreater() ? 40 : 30),
                                                       height: CGFloat.greatestFiniteMagnitude),
                                          options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                          context: nil)
            return rect.size.height + 15
            
        } else if let title = sectionModel?.htmlTitleForFooter(),
            (height < 0 || height > 1)
        {
            let font = sectionModel?.htmlTitleFooterFont() ?? UIFont.systemFont(ofSize: 14.0)
            
            let titleStripped = title.stripHTML()
            
            let rect = titleStripped.boundingRect(with: CGSize(width: tableView.bounds.size.width - 30,
                                                               height: CGFloat.greatestFiniteMagnitude),
                                                  options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                  attributes: [NSAttributedString.Key.font : font],
                                                  context: nil)
            return rect.size.height + 50
        }
        if height >= 0 {
            return height
        }
        return -1
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionModel = self.sectionViewModelForSection(section)
        
        var deltaTitle: String? = nil
        if let _ = tableView.tableHeaderView,
            section == 0,
            self.tableView(tableView, viewForHeaderInSection: 0) == nil
        {
            if useRoundedModeActive && roundedModeCanBeActive() {
                deltaTitle = " "
            } else {
                deltaTitle = nil
            }
        }
        
        let title = sectionModel?.attributedTitleForHeader()
        if title != nil {
            return deltaTitle
        }
        let header = sectionModel?.titleForHeaderDefault()
        if header != nil {
            return header
        }
        return sectionModel?.titleForHeader() ?? deltaTitle
    }
    
    public func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        let sectionModel = self.sectionViewModelForSection(section)
        let title = sectionModel?.attributedTitleForFooter()
        if title != nil {
            return nil
        }
        let footer = sectionModel?.titleForFooterDefault()
        if footer != nil {
            return footer
        }
        return sectionModel?.titleForFooter()
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let sectionModel = self.sectionViewModelForSection(indexPath.section)
        let h = sectionModel?.heightForRowDefault(row: indexPath.row) ?? 0
        if h >= 0 {
            return h
        }
        return sectionModel?.heightForRow(row: indexPath.row) ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionModel = self.sectionViewModelForSection(indexPath.section)
        return sectionModel?.cellForRowAtIndexPath(indexPath: indexPath) ??
            UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: nil)
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionModel = self.sectionViewModelForSection(indexPath.section)
        sectionModel?.didSelectRowAtIndexPath(indexPath: indexPath)
    }
    
    public func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        let sectionModel = self.sectionViewModelForSection(indexPath.section)
        sectionModel?.accessoryButtonTappedForRowWith(indexPath: indexPath)
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionModel = self.sectionViewModelForSection(section)
        
        guard sectionModel?.titleForHeaderDefault() == nil else {
            return nil
        }
        
        let view = sectionModel?.viewForHeader()
        if (view != nil) {
            return view
        }
        
        if let title = sectionModel?.attributedTitleForHeader() {
            if self.labelDictionary == nil {
                self.labelDictionary = [:]
            }
            var view  = self.labelDictionary?[section]
            var label = view?.subviews.first as? UILabel
            if view == nil {
                view = UIView(frame: tableView.frame)
                view?.backgroundColor = UIColor.clear
                label = UILabel(frame: tableView.frame)
                label?.numberOfLines = 0
                view?.addSubview(label!)
                view?.addConstraint(NSLayoutConstraint(item: view!,
                                                       attribute: NSLayoutConstraint.Attribute.top,
                                                       relatedBy: NSLayoutConstraint.Relation.equal,
                                                       toItem: label,
                                                       attribute: NSLayoutConstraint.Attribute.top,
                                                       multiplier: 1,
                                                       constant: 0))
                view?.addConstraint(NSLayoutConstraint(item: view!,
                                                       attribute: NSLayoutConstraint.Attribute.height,
                                                       relatedBy: NSLayoutConstraint.Relation.equal,
                                                       toItem: label,
                                                       attribute: NSLayoutConstraint.Attribute.height,
                                                       multiplier: 1,
                                                       constant: 0))
                view?.addConstraint(NSLayoutConstraint(item: label!,
                                                       attribute: NSLayoutConstraint.Attribute.left,
                                                       relatedBy: NSLayoutConstraint.Relation.equal,
                                                       toItem: view,
                                                       attribute: NSLayoutConstraint.Attribute.left,
                                                       multiplier: 1,
                                                       constant: 15))
                view?.addConstraint(NSLayoutConstraint(item: view!,
                                                       attribute: NSLayoutConstraint.Attribute.right,
                                                       relatedBy: NSLayoutConstraint.Relation.equal,
                                                       toItem: label,
                                                       attribute: NSLayoutConstraint.Attribute.right,
                                                       multiplier: 1,
                                                       constant: 15))
                label?.translatesAutoresizingMaskIntoConstraints = false
                
                self.labelDictionary?[section] = view
            }
            label?.attributedText = title
            return view
            
        }   else {
            
            if let title = sectionModel?.htmlTitleForHeader() {
                let view  = setupSectionModel(sectionModel, forRTLabelHeader: true, tableView:tableView, section:section, heightOnly:false)
                let label = view?.subviews.first as? RTLabel_Viperforswift
                label?.text = title
                return view
            }
        }
        return nil
    }
    
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let sectionModel = self.sectionViewModelForSection(section)
        
        guard sectionModel?.titleForFooterDefault() == nil else {
            return nil
        }
        
        let view = sectionModel?.viewForFooter()
        if (view != nil) {
            return view
        }
        
        if let title = sectionModel?.attributedTitleForFooter() {
            if self.labelFooterDictionary == nil {
                self.labelFooterDictionary = [:]
            }
            var view  = self.labelFooterDictionary?[section]
            var label = view?.subviews.first as? UILabel
            if view == nil {
                view = UIView(frame: tableView.frame)
                view?.backgroundColor = UIColor.clear
                label = UILabel(frame: tableView.frame)
                label?.numberOfLines = 0
                view?.addSubview(label!)
                view?.addConstraint(NSLayoutConstraint(item: view!,
                                                       attribute: NSLayoutConstraint.Attribute.top,
                                                       relatedBy: NSLayoutConstraint.Relation.equal,
                                                       toItem: label,
                                                       attribute: NSLayoutConstraint.Attribute.top,
                                                       multiplier: 1,
                                                       constant: 0))
                view?.addConstraint(NSLayoutConstraint(item: view!,
                                                       attribute: NSLayoutConstraint.Attribute.height,
                                                       relatedBy: NSLayoutConstraint.Relation.equal,
                                                       toItem: label,
                                                       attribute: NSLayoutConstraint.Attribute.height,
                                                       multiplier: 1,
                                                       constant: 0))
                view?.addConstraint(NSLayoutConstraint(item: label!,
                                                       attribute: NSLayoutConstraint.Attribute.left,
                                                       relatedBy: NSLayoutConstraint.Relation.equal,
                                                       toItem: view,
                                                       attribute: NSLayoutConstraint.Attribute.left,
                                                       multiplier: 1,
                                                       constant: 15))
                view?.addConstraint(NSLayoutConstraint(item: view!,
                                                       attribute: NSLayoutConstraint.Attribute.right,
                                                       relatedBy: NSLayoutConstraint.Relation.equal,
                                                       toItem: label,
                                                       attribute: NSLayoutConstraint.Attribute.right,
                                                       multiplier: 1,
                                                       constant: 15))
                label?.translatesAutoresizingMaskIntoConstraints = false
                
                self.labelFooterDictionary?[section] = view
            }
            label?.attributedText = title
            return view
            
            
        } else {
            
            if let title = sectionModel?.htmlTitleForFooter() {
                let view  = setupSectionModel(sectionModel, forRTLabelHeader: false, tableView:tableView, section:section, heightOnly:false)
                let label = view?.subviews.first as? RTLabel_Viperforswift
                label?.text = title
                return view
            }
        }
        return nil;
    }
    
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let sectionModel = self.sectionViewModelForSection(indexPath.section)
        return sectionModel?.canEditRowAtIndexPath(indexPath: indexPath) ?? false
    }
    
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            // Delete the row from the data source
            let sectionModel = self.sectionViewModelForSection(indexPath.section)
            sectionModel?.commit(editingStyle: editingStyle, forRowAtIndexPath: indexPath)
        }
    }
    
    public func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let sectionModel = self.sectionViewModelForSection(sourceIndexPath.section)
        sectionModel?.moveRowAtIndexPath(fromIndexPath: sourceIndexPath, toIndexPath: destinationIndexPath)
    }
    
    public func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        let sectionModel = self.sectionViewModelForSection(indexPath.section)
        return sectionModel?.canMoveRowAtIndexPath(indexPath: indexPath) ?? false
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.tableView(tableView, roundOff: cell, forRowAt: indexPath)
        //        roundOffAllVisibleCells()
    }
    
    //    public func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        roundOffAllVisibleCells()
    //    }
    
}

//MARK: - Rouded mode implementation helpers
private extension TableLayoutManager {
    
    func roundedModeCanBeUsedOnDevice() -> Bool {
        return iPad() || iPhone6PlusAndGreater()
    }
    
    func roundedModeCanBeActive() -> Bool {
        return iPad() || iPhone6PlusAndGreaterLandscape()
    }
    
    @objc func orientationDidChange() {
        if useRoundedMode {
            doRoundedMode(useRoundedModeActive: roundedModeCanBeActive())
            roundOffAllVisibleCells()
            if roundedModeCanBeUsedOnDevice() {
                tableView?.reloadData()
            }
        }
    }
    
    func doRoundedMode(useRoundedModeActive: Bool) {
        guard let tableView = tableView else {
            return
        }
        self.useRoundedModeActive = useRoundedModeActive
        if useRoundedModeActive {
            if self.separatorColor == nil {
                self.separatorColor = tableView.separatorColor
                self.separatorStyle = tableView.separatorStyle
            }
            tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
            tableView.separatorColor = UIColor.clear
            
            if let tableHeaderView = tableView.tableHeaderView {
                let view = /*tableView.superview ?? */tableView
                let width = (!isLandscape() ?
                    min(view.bounds.width, view.bounds.height) :
                    max(view.bounds.width, view.bounds.height))// - (tableView.superview != nil ? self.leftMargin + self.rightMargin : 0)
                let height = tableHeaderView.bounds.size.height
                
                let shape = CAShapeLayer.init()
                shape.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 10, width: width, height: height - 10),
                                          byRoundingCorners: UIRectCorner.allCorners,
                                          cornerRadii: CGSize(width: 10, height: 10)).cgPath
                tableHeaderView.layer.mask = shape
                tableHeaderView.layer.masksToBounds = true
            }
            
        } else {
            if let tableHeaderView = tableView.tableHeaderView {
                tableHeaderView.layer.mask = nil
                tableHeaderView.layer.masksToBounds = false
            }
            if self.separatorColor != nil {
                tableView.separatorStyle = self.separatorStyle ?? tableView.separatorStyle
                tableView.separatorColor = self.separatorColor
            }
        }
        
        if let view = tableView.superview {
            let constraints = view.constraints.filter({ (c) -> Bool in
                if let first = c.firstItem,
                    let second = c.secondItem
                {
                    if type(of: first) == UITableView.self && c.firstAttribute == NSLayoutConstraint.Attribute.leading
                    {
                        if useRoundedModeActive && self.origLeftMargin == nil {
                            self.origLeftMargin = c.constant
                        }
                        return true
                    }
                    if type(of: second) == UITableView.self && c.secondAttribute == NSLayoutConstraint.Attribute.trailing
                    {
                        if useRoundedModeActive && self.origRightMargin == nil {
                            self.origRightMargin = c.constant
                        }
                        return true
                    }
                }
                return false
            })
            if useRoundedModeActive || (self.origLeftMargin != nil && self.origRightMargin != nil) {
                view.removeConstraints(constraints)
                if useRoundedModeActive || self.origLeftMargin != nil {
                    view.addConstraint(NSLayoutConstraint(item: tableView,
                                                          attribute: NSLayoutConstraint.Attribute.leading,
                                                          relatedBy: NSLayoutConstraint.Relation.equal,
                                                          toItem: view,
                                                          attribute: NSLayoutConstraint.Attribute.leading,
                                                          multiplier: 1,
                                                          constant: useRoundedModeActive ? leftMargin : origLeftMargin ?? 0)
                    )
                }
                if useRoundedModeActive || self.origRightMargin != nil {
                    view.addConstraint(NSLayoutConstraint(item: view,
                                                          attribute: NSLayoutConstraint.Attribute.trailing,
                                                          relatedBy: NSLayoutConstraint.Relation.equal,
                                                          toItem: tableView,
                                                          attribute: NSLayoutConstraint.Attribute.trailing,
                                                          multiplier: 1,
                                                          constant: useRoundedModeActive ? rightMargin : origRightMargin ?? 0)
                    )
                }
            }
        }
    }
    
    func firstCell(tableView: UITableView, cell: UITableViewCell, forRowAt indexPath: IndexPath) -> Bool  {
        let nRows = self.tableView(tableView, numberOfRowsInSection: indexPath.section)
        for i in 0..<nRows {
            let height = self.tableView(tableView, heightForRowAt: indexPath)
            if height != 0 {
                return indexPath.row == i
            }
        }
        return false;
    }
    
    func lastCell(tableView: UITableView, cell: UITableViewCell, forRowAt indexPath: IndexPath) -> Bool  {
        let nRows = self.tableView(tableView, numberOfRowsInSection: indexPath.section)
        for i in stride(from: nRows - 1, through: 0, by: -1) {
            let height = self.tableView(tableView, heightForRowAt: indexPath)
            if height != 0 {
                return indexPath.row == i
            }
        }
        return false;
    }
    
    func roundOffAllVisibleCells() {
        guard roundedModeCanBeUsedOnDevice(),
            let table = tableView else
        { return }
        if !self.useRoundedModeActive {
            for cell in table.visibleCells {
                // remove old separatore (if reuse cell)
                if let sublayers = cell.layer.sublayers {
                    for i in stride(from: sublayers.count - 1, through: 0, by: -1) {
                        let layer = sublayers[i]
                        if layer.name == "SeparatorLayer" {
                            layer.removeFromSuperlayer()
                        }
                    }
                }
                //            if let indexPath = table.indexPath(for: cell) {
                //                tableView(table, roundOff: cell, forRowAt: indexPath)
                //            }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, roundOff cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard roundedModeCanBeUsedOnDevice() else
        { return }
        
        if self.useRoundedModeActive {
            
            let view = /*tableView.superview ?? */tableView
            let width = (!isLandscape() ?
                min(view.bounds.width, view.bounds.height) :
                max(view.bounds.width, view.bounds.height)) //- (tableView.superview != nil ? self.leftMargin + self.rightMargin : 0)
            let height = cell.bounds.size.height
            
            var addSeparator = true
            let numberOfRows = self.tableView(tableView, numberOfRowsInSection: indexPath.section)
            
            if numberOfRows == 1 { //only one cell in section
                
                let shape = CAShapeLayer.init()
                shape.path = UIBezierPath(
                    roundedRect: CGRect(x: 0, y: 0, width: width, height: height),
                    byRoundingCorners: UIRectCorner.allCorners,
                    cornerRadii: CGSize(width: 10, height: 10)).cgPath
                cell.layer.mask = shape
                cell.layer.masksToBounds = true
            }
            
            let firstCell = self.firstCell(tableView:tableView, cell: cell, forRowAt: indexPath)
            let lastCell = self.lastCell(tableView:tableView, cell: cell, forRowAt: indexPath)
            
            if (firstCell || lastCell) {
                if (firstCell && !lastCell) {
                    let shape = CAShapeLayer.init()
                    shape.path = UIBezierPath(
                        roundedRect: CGRect(x: 0, y: 0, width: width, height: height),
                        byRoundingCorners: UIRectCorner.topLeft.union(UIRectCorner.topRight),
                        cornerRadii: CGSize(width: 15, height: 15)).cgPath
                    cell.layer.mask = shape
                    cell.layer.masksToBounds = true
                } else if (!firstCell && lastCell) {
                    let shape = CAShapeLayer.init()
                    shape.path = UIBezierPath(
                        roundedRect: CGRect(x: 0, y: 0, width: width, height: height),
                        byRoundingCorners: UIRectCorner.bottomLeft.union(UIRectCorner.bottomRight),
                        cornerRadii: CGSize(width: 15, height: 15)).cgPath
                    cell.layer.mask = shape
                    cell.layer.masksToBounds = true
                    addSeparator = false
                } else {
                    let shape = CAShapeLayer.init()
                    shape.path = UIBezierPath(
                        roundedRect: CGRect(x: 0, y: 0, width: width, height: height),
                        byRoundingCorners: UIRectCorner.allCorners,
                        cornerRadii: CGSize(width: 10, height: 10)).cgPath
                    cell.layer.mask = shape
                    cell.layer.masksToBounds = true
                    addSeparator = false
                }
            } else {
                let shape = CAShapeLayer.init()
                shape.path = UIBezierPath(rect: cell.bounds).cgPath
                cell.layer.mask = shape
                cell.layer.masksToBounds = true
            }
            
            // remove old separatore (if reuse cell)
            if let sublayers = cell.layer.sublayers {
                for i in stride(from: sublayers.count - 1, through: 0, by: -1) {
                    let layer = sublayers[i]
                    if layer.name == "SeparatorLayer" {
                        layer.removeFromSuperlayer()
                    }
                }
            }
            
            if (addSeparator) {
                if indexPath.row < numberOfRows - 1 { //not last cell
                    let lineLayer = CALayer.init()
                    let lineHeight: CGFloat = 1.0 / UIScreen.main.scale
                    lineLayer.frame = CGRect(x: cell.bounds.minX + cell.separatorInset.left, y: height - lineHeight, width: width - cell.separatorInset.left, height: lineHeight)
                    lineLayer.backgroundColor = self.separatorColor?.cgColor
                    lineLayer.name = "SeparatorLayer"
                    cell.layer.addSublayer(lineLayer)
                }
            }
        } else {
            cell.layer.mask = nil
            cell.layer.masksToBounds = false
        }
    }
}

//MARK: - Implementation helpers
private extension TableLayoutManager {
    
    func iPad() -> Bool {
        return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
    }
    
    func isLandscape() -> Bool {
        return UIApplication.shared.statusBarOrientation.isLandscape
    }
    
    func iPhone6PlusAndGreater() -> Bool {
        let screenSize = UIScreen.main.bounds.size
        return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone &&
            max(screenSize.height, screenSize.width) >= 736)
    }
    
    func iPhone6PlusAndGreaterLandscape() -> Bool {
        let screenSize = UIScreen.main.bounds.size
        return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone &&
            screenSize.width >= 736
    }
    
    static func tableFooterColor() -> UIColor {
        return UIColor(red: 110.0 / 255.0, green: 110.0 / 255.0, blue: 115.0 / 255.0, alpha: 1.0)
    }
    
    func setupSectionModel(_ item: TableSectionViewModel?, forRTLabelHeader header: Bool, tableView: UITableView, section: Int, heightOnly: Bool) -> UIView?  {
        
        var view: UIView? = nil
        
        if (!heightOnly) {
            if (header) {
                if self.rtlabelDictionary == nil {
                    self.rtlabelDictionary = [:]
                }
                view  = self.rtlabelDictionary?[section]
            } else {
                if self.rtlabelFooterDictionary == nil {
                    self.rtlabelFooterDictionary = [:]
                }
                view  = self.rtlabelFooterDictionary?[section]
            }
        }
        
        var label = view?.subviews.first as? RTLabel_Viperforswift
        
        if view == nil {
            view = UIView(frame: tableView.frame)
            view?.backgroundColor = UIColor.clear
            label = RTLabel_Viperforswift(frame: tableView.frame)
            view?.addSubview(label!)
            
            view?.addConstraint(NSLayoutConstraint(item: label!,
                                                   attribute: NSLayoutConstraint.Attribute.top,
                                                   relatedBy: NSLayoutConstraint.Relation.equal,
                                                   toItem: view,
                                                   attribute: NSLayoutConstraint.Attribute.top,
                                                   multiplier: 1,
                                                   constant: 15))
            
            view?.addConstraint(NSLayoutConstraint(item: label!,
                                                   attribute: NSLayoutConstraint.Attribute.height,
                                                   relatedBy: NSLayoutConstraint.Relation.equal,
                                                   toItem: view,
                                                   attribute: NSLayoutConstraint.Attribute.height,
                                                   multiplier: 1,
                                                   constant: 0))
            
            view?.addConstraint(NSLayoutConstraint(item: label!,
                                                   attribute: NSLayoutConstraint.Attribute.left,
                                                   relatedBy: NSLayoutConstraint.Relation.equal,
                                                   toItem: view,
                                                   attribute: NSLayoutConstraint.Attribute.left,
                                                   multiplier: 1,
                                                   constant: 15))
            
            view?.addConstraint(NSLayoutConstraint(item: view!,
                                                   attribute: NSLayoutConstraint.Attribute.right,
                                                   relatedBy: NSLayoutConstraint.Relation.equal,
                                                   toItem: label,
                                                   attribute: NSLayoutConstraint.Attribute.right,
                                                   multiplier: 1,
                                                   constant: 15))
            
            label?.translatesAutoresizingMaskIntoConstraints = false
            
            var optionalFont: UIFont? =  nil
            if header {
                optionalFont = item?.htmlTitleHeaderFont()
            } else {
                optionalFont = item?.htmlTitleFooterFont()
            }
            if optionalFont != nil {
                label?.font = optionalFont!
            } else {
                label?.font = UIFont.systemFont(ofSize: 14.0)
            }
            
            label?.textColor = TableLayoutManager.tableFooterColor()
            
            if let htmlLinkColorName = item?.htmlLinkColorName() {
                label?.linkAttributes = ["color" : htmlLinkColorName]
            } else {
                label?.linkAttributes = ["color" : "blue"]
            }
            
            label?.selectedLinkAttributes = ["color" : "darkGray"]
            
            if (!heightOnly) {
                if (header) {
                    self.rtlabelDictionary?[section] = view
                } else {
                    self.rtlabelFooterDictionary?[section] = view
                }
            }
        }
        label?.delegate = self
        label?.tag = section
        view?.clipsToBounds = false
        label?.clipsToBounds = false
        return view
    }
    
}


extension TableLayoutManager: RTLabelDelegate {
    public func rtLabel(_ rtLabel: Any!, didSelectLinkWith url: URL!, text: String!) {
        if let section = (rtLabel as? UIView)?.tag,
            let sectionModel = self.sectionViewModelForSection(section),
            let block = sectionModel.htmlLinkBlock
        {
            block(url, text)
        }
    }
}


//MARK: - TableSectionViewModel helpers
fileprivate extension TableSectionViewModel {
    
    fileprivate func heightForHeaderDefault() -> CGFloat {
        if rowFilterBlockCalledAndFilteredOut() {
            return 0.1
        }
        return -1
    }
    
    /// can be overriden
    fileprivate func heightForFooterDefault() -> CGFloat {
        if rowFilterBlockCalledAndFilteredOut() {
            return 0.1
        }
        return -1
    }
    
    /// can be overriden
    fileprivate func titleForHeaderDefault() -> String? {
        if rowFilterBlockCalledAndFilteredOut() {
            return ""
        }
        return nil
    }
    
    /// can be overriden
    fileprivate func titleForFooterDefault() -> String? {
        if rowFilterBlockCalledAndFilteredOut() {
            return ""
        }
        return nil
    }
    
    /// can be overriden
    fileprivate func heightForRowDefault(row: Int) -> CGFloat {
        if rowFilterBlockCalledAndFilteredOut() {
            return 0
        }
        return -1
    }
}

